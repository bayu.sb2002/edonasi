<?php if( ! defined('BASEPATH'))exit('No direct script access allowed');
    
class Model_config extends CI_Model{

    public function ID_Klinik(){
       return "001"; // harus diset sesuai cabang PHC
    }

    public function ID_Inhealth(){
        return "001";
    }

    public function WebService(){
        return "http://192.168.0.7/restsirs/";
        // return "http://".$_SERVER['HTTP_HOST']."/restsirs/";
        // return "http://10.10.10.110/restsirs/";
    	// return "http://192.168.0.8/restsirs/";
    }

    public function IDPusat(){
    	return "001";
    } 

    public function PATHLAB(){
        // return "\\\\10.10.10.114\\dummy\\TesDoc\\";
        return "\\\\192.168.0.4\\dataRSPS\\KLINIK\\Laborat\\Output\\";
        // return "\\\\192.168.0.4\\dataRSPS\\KLINIK\\Laborat\\Output\\";
        
        // return "http://10.10.10.110/restsirs/";
        // return "http://192.168.0.8/restsirs/";
    }

    public function IDFaskesBPJS(){
        // $id_klinik      = $this->ID_Klinik(); // harus diset sesuai cabang PHC
    	$id_klinik 		= "001"; // harus diset sesuai cabang PHC
    	$kd_faskes 		= "";

    	if($id_klinik == "001"){ // RS PHC
    		$kd_faskes	= "1301R007";
            $nm_faskes  = "RS PHC";
        }else if($id_klinik == "002"){ //Klinik Tanjung Perak
            $kd_faskes  = "0217B094";
            $nm_faskes  = "Klinik Tanjung Perak";
        }else if($id_klinik == "003"){ //Klinik Benowo
            $kd_faskes  = "0217B095";
            $nm_faskes  = "Klinik Benowo";
        }else if($id_klinik == "004"){ //Klinik Kebraon
            $kd_faskes  = "0217B096";
            $nm_faskes  = "Klinik Kebraon";
        }else if($id_klinik == "005"){ //Klinik Tanjung Emas
            $kd_faskes  = "0173B013";
            $nm_faskes  = "Klinik Tanjung Emas";
    	}

        $data           = array(
            "kd_faskes"         => $kd_faskes,
            "nm_faskes"         => $nm_faskes
        );

        // return $kd_faskes;
    	return $data;
    }

    public function KodeTransFarmasi(){
        // $id_klinik      = $this->ID_Klinik();
        $id_klinik      = "001";
        if($id_klinik == "001"){ // RS PHC
            $kd_farmasi  = "A";
        }else if($id_klinik == "002"){ // Klinik Tanjung Perak
            $kd_farmasi  = "B";
        }else if($id_klinik == "003"){ // Klinik Benowo
            $kd_farmasi  = "C";
        }else if($id_klinik == "004"){ // Klinik Kebraon
            $kd_farmasi  = "D";
        }else if($id_klinik == "005"){ // Klinik Tanjung Emas
            $kd_farmasi  = "E";
        }else if($id_klinik == "006"){ // Klinik Pedurungan
            $kd_farmasi  = "F";
        }else if($id_klinik == "007"){ // Klinik Banjarmasin
            $kd_farmasi  = "G";
        }
        return $kd_farmasi;
    }

    public function IDFaskesINHEALTH(){
        // $id_klinik      = $this->ID_Klinik(); // harus diset sesuai cabang PHC
        $id_klinik      = "001"; // harus diset sesuai cabang PHC
        $kd_faskes      = "";

        if($id_klinik == "001"){ // RS PHC
            $kd_faskes  = "1301R004"; 
            $token1     = "C+sUTahO5o8Mz//0kA3XAA=="; //dummy
            $token      = "fVmnQOr2Qyfx1QZ+9gbkpw==";
        }else if($id_klinik == "002"){ //Klinik Tanjung Perak
            $kd_faskes  = "1301K034";
            $token1     = "8n/rbdvmelXAGZMxOYCLCQ=="; //dummy
            $token      = "dtMsO+moeMXthUwLPbobFg==";
        }else if($id_klinik == "003"){ //Klinik Benowo
            $kd_faskes  = "1301K021";
            $token1     = "2baOpC7o73QYuobLuWh2sg=="; //dummy
            $token      = "nvevhF6G3LuEiPaHZU09qw==";
        }else if($id_klinik == "004"){ //Klinik Kebraon
            $kd_faskes  = "1301K014";
            $token1     = "2S2QbWXsUAoBCk+FI3YMxw=="; // dummy
            $token      = "j8R0BUR3+yT2bkScRj8/xg==";
        }else if($id_klinik == "005"){ //Klinik Tanjung Emas
            $kd_faskes  = "1103K006";
            $token1      = "65+dFf6+zIb3nAZG8B0JnA=="; //dummy
            $token      = "S81Efn6oDMKTWk3iUE3dFQ==";
        }else if($id_klinik == "006"){ //Klinik Pedurungan
            $kd_faskes  = "1103K007";
            $token1     = "s5E5IliflrNUaGz1PF5FUg=="; // dummy
            $token      = "EaMBQK4sRnLx/Gwnct/56g==";
        }else if($id_klinik == "002GIG"){ //Klinik Gigi Perak 
            $kd_faskes  = "1301G028";
            $token1     = "oyd51QxqI+5Sj6Etyp1yEw=="; // dummy
            $token      = "vno7H8xAuTWNqUDVM4xAuw==";
        }else if($id_klinik == "003GIG"){ //Klinik Gigi Benowo
            $kd_faskes  = "1301G026";
            $token1     = "oCxWdnwxdvYo0qHDFHroBQ=="; // dummy
            $token      = "jROJnM9//CJj/yE7QNUhrw==";
        }else if($id_klinik == "004GIG"){ //Klinik Gigi Kebraon
            $kd_faskes  = "1301G027";
            $token1     = "3aKCisuAxrd2XaFNOVWulA=="; // dummy
            $token      = "u+P1DPcXzz5EVClnf4WbbA==";
        }

        $data           = array(
            "kd_provider"       => $kd_faskes,
            "token"             => $token,
        );

        return json_encode($data);
    }

    public function IDUNITFaktur($id_klinik){
        if($id_klinik == "001"){ // RS PHC
            $kdunit     = "RS";
            $nm_unit    = "RS. PHC Surabaya";
        }else if($id_klinik == "002"){ //Klinik Tanjung Perak
            $kdunit     = "PERAK";
            $nm_unit    = "Klinik Tanjung Perak";
        }else if($id_klinik == "003"){ //Klinik Benowo
            $kdunit     = "BENOWO";
            $nm_unit    = "Klinik Benowo";
        }else if($id_klinik == "004"){ //Klinik Kebraon
            $kdunit     = "KEBRAON";
            $nm_unit    = "Klinik Kebraon";
        }else if($id_klinik == "005"){ //Klinik Tanjung Emas
            $kdunit     = "EMAS";
            $nm_unit    = "Klinik Tanjung Emas";
        }else if($id_klinik == "006"){ //Klinik Pedurungan
            $kdunit     = "PEDURUNGAN";
            $nm_unit    = "Klinik Pedurungan";
        }else if($id_klinik == "007"){ //Klinik Banjarmasin
            $kdunit     = "BANJARMASIN";
            $nm_unit    = "Klinik Banjarmasin";
        }

        $data           = array(
            "kdunit"        => $kdunit,
            "nm_unit"       => $nm_unit,
        );

        return json_encode($data);
    }
}
?>