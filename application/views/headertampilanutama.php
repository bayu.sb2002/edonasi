<?php 
date_default_timezone_set('Asia/Jakarta'); 
$_SESSION["hrd_startMeasure"] = microtime(TRUE);
ini_set("memory_limit", "512M"); // or you could use 1G
//echo $this->benchmark->memory_usage();
$this->benchmark->mark('code_start');
?>

<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon" />
    <title>SI Rawat Inap - PHC Surabaya</title>
  
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url();?>assets/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">

  <!-- Callout Bootstrap -->
    <link href="<?php echo base_url();?>assets/css/bootstrap-callout.css" rel="stylesheet" type="text/css">    

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Datatable bootstrap -->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatable/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatable/responsive.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/datatable/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/datatable/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/datatable/dataTables.responsive.min.js"></script>
  
  
  <!-- Jquery UI autocomplete -->
  <!--<link rel="stylesheet" href="<?php echo base_url();?>assets/autocomplete/jquery-ui.css">
    <script src="<?php echo base_url();?>assets/autocomplete/jquery-ui.js"></script>
  -->

    <!-- Datepicker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/datepicker/bootstrap-datetimepicker.css">
    <script src="<?php echo base_url();?>assets/datepicker/moment-with-locales.js"></script>
    <script src="<?php echo base_url();?>assets/datepicker/bootstrap-datetimepicker.js"></script>
  

    <!-- Selection -->
     <link rel="stylesheet" href="<?php echo base_url();?>assets/selection/bootstrap-select.min.css">
     <script src="<?php echo base_url();?>assets/selection/bootstrap-select.min.js"></script>

     <!-- checkbox -->
     <!--<link rel="stylesheet" href="<?php echo base_url();?>assets/checkbox/build.css">-->

     <!-- Notify JS -->
     <script src="<?php echo base_url();?>assets/notify/notify.js"></script>

     <!-- Date JS -->
     <script src="<?php echo base_url();?>assets/js/date.js"></script>


     <!-- Select 2 source -->
     <link rel="stylesheet" href="<?php echo base_url();?>assets/select2/select2.min.css">
     <script src="<?php echo base_url();?>assets/select2/select2.min.js"></script>
     <!-- <script src="<?php echo base_url();?>assets/select2/anchor.min.js"></script> -->
     <!-- <script src="<?php echo base_url();?>assets/select2/prettify.min.js"></script> -->

     <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/datatable-export/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/datatable-export/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/datatable-export/buttons.html5.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/datatable-export/buttons.dataTables.min.css">
 
    <!-- Inisialisasi datatable -->
    <script type="text/javascript">
      $(document).ready(function() {
        $('#tabelList').DataTable({
          responsive: {
              details: {
                  type: 'column',
                  target: 'tr'
              }
          },
          columnDefs: [ {
              className: 'control',
              orderable: false,
              targets:   0
          } ],
          language: {
            sSearch      : '<span> Cari Data </span> _INPUT_',
            lengthMenu   : 'Tampilkan data  _MENU_  baris',
            info         : 'Menampilkan halaman _PAGE_ dari total _PAGES_ halaman',
            infoEmpty    : 'Tidak ada data yang ditampilkan',
            infoFiltered : '(Dicari dari total _MAX_ data)',
            zeroRecords  : 'Maaf tidak ada hasil manapun dari data yang dicari',
            oPaginate: {
                sNext     : 'Berikutnya',
                sPrevious : 'Sebelumnya',
                sLast     : 'Halaman Terakhir',
                sFirst    : 'Halaman Awal'

            }
          },
          order: []
        });


        $('#tabelList5').DataTable({
          responsive: {
              details: {
                  type: 'column',
                  target: 'tr'
              }
          },
          columnDefs: [ {
              className: 'control',
              orderable: false,
              targets:   0
          } ],
          language: {
            sSearch      : '<span> Cari Data </span> _INPUT_',
            lengthMenu   : 'Tampilkan data  _MENU_  baris',
            info         : 'Menampilkan halaman _PAGE_ dari total _PAGES_ halaman',
            infoEmpty    : 'Tidak ada data yang ditampilkan',
            infoFiltered : '(Dicari dari total _MAX_ data)',
            zeroRecords  : 'Maaf tidak ada hasil manapun dari data yang dicari',
            oPaginate: {
                sNext     : 'Berikutnya',
                sPrevious : 'Sebelumnya',
                sLast     : 'Halaman Terakhir',
                sFirst    : 'Halaman Awal'

            }
          },
          order: []
        });


        $(function () {
        $('[data-toggle="tooltip"]').tooltip({
          delay: { "show": 0, "hide": 50 }
        });
      });

    });
    </script>
<style>
/* CSS CLOCK */

#Date {
    font-family: 'BebasNeueRegular', Arial, Helvetica, sans-serif;
    font-size: 36px;
    text-align: center;
    text-shadow: 0 0 5px #00c6ff;
}

#modeljam {
    display: inline;
    font-size: 10em;
    text-align: center;
    font-family: 'BebasNeueRegular', Arial, Helvetica, sans-serif;
    text-shadow: 0 0 5px #00c6ff;
}

/*untuk format atribut*/
.lurus input{
  float:left;
}

/* Custom Warna Tabel */

.side-nav {
   /* background-image: url("<?php echo base_url();?>assets/img/bg-sidebar3.png");
    background-size: 320px;*/
    background-image: url("<?php echo base_url();?>assets/img/pattern2.jpg");
    background-position: top left;
    background-repeat : no-repeat;
    background-repeat: repeat-y;
    background-size: 100% auto;
  }

.side-nav:before{
  filter: blur(5px) brightness(0.5);
}

.child-row{
white-space: pre-wrap;      /* CSS3 */   
 white-space: -moz-pre-wrap; /* Firefox */    
 white-space: -pre-wrap;     /* Opera <7 */   
 white-space: -o-pre-wrap;   /* Opera 7 */    
 word-wrap: break-word;      /* IE */
}

.page-header{
margin-top: 10px;
margin-bottom: 7px;
padding-bottom: 3px;
}

.modal-body{
margin-top: 0px;
padding-top: 0px;
}

#page-wrapper{
    animation: shadowout 2s;
    -moz-animation: shadowout 2s; /* Firefox */
    -webkit-animation: shadowout 2s; /* Safari and Chrome */
    -o-animation: shadowout 2s; /* Opera */
    border-bottom: solid 2px #FFDC00; 
}

.content, .page-header {
    margin-top: 0px;
    animation: fadein 1s;
    -moz-animation: fadein 1s; /* Firefox */
    -webkit-animation: fadein 1s; /* Safari and Chrome */
    -o-animation: fadein 1s; /* Opera */
}

@keyframes fadein {
    from {opacity:0;}
    to {opacity:1;}
}
@-moz-keyframes fadein { /* Firefox */
    from {opacity:0;}
    to {opacity:1;}
}
@-webkit-keyframes fadein { /* Safari and Chrome */
    from {opacity:0;}
    to {opacity:1;}
}

@keyframes shadowout {
    from {text-shadow:0 0 10px white;}
    to {text-shadow:0 0 0 white;}
}

  /* Data warna untuk dashboarding */  
  .text-judul{
    font-size: 20px;
  }

  .panel-footer{
    background-color: #F2F2F2;
    color: #7A7A7A;
  }

  .bg-black{
    background-color: #111111;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;
  }

  .bg-blue{
    background-color: #0074D9;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;  
  }

  .bg-navy{
    background-color: #001f3f;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;  
  }


  .bg-aqua{
    background-color: #7FDBFF;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;
  }

  .bg-teal{
    background-color: #39CCCC;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;
  }

  .bg-olive{
    background-color: #3D9970;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;
  }

  .bg-green{
    background-color: #2ECC40;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;
  }
  
   .bg-lime{
    background-color: #01FF70;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;

   }

   .bg-yellow{
    background-color: #FFDC00;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;

   }

   .bg-orange{
    background-color: #FF851B;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;

   }

   .bg-red{
    background-color: #FF4136;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;

   }

   .bg-maroon{
    background-color: #85144b ;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;

   }

   .bg-fuschia{
    background-color: #F012BE ;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;

   }

   .bg-purple{
    background-color: #B10DC9 ;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;

   }

   .bg-gray{
    background-color: #AAAAAA ;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;

   }

   .bg-silver{
    background-color: #DDDDDD ;
    color: #FFFFFF ;
    border : 0px;
    border-radius: 5px;
   }


  /* footer */
  /* Sticky footer styles
  -------------------------------------------------- */
  html {
    position: relative;
    min-height: 100%;
  }
  body {
    /* Margin bottom by footer height */
    margin-bottom: 60px;
  }
  .footer {
    position: absolute;
    bottom: 0;
    /* Set the fixed height of the footer here */
    height: 60px;
  }


  body > .container {
    padding: 60px 15px 0;
  }
  .container .text-muted {
    margin: 20px 0;
  }


  code {
    font-size: 80%;
  }


  .topbar {
    font-size: 0.8em;
    display: block;
    position: absolute;
    top: 0.2em;
    right: -0.2em;    
    color: black;
    background-color:#FFDC00;
}



.sidebar {
    font-size: 0.8em;
    display: inline-block;
    width: 20px;
    margin-left: 5px;
    color: black;
    background-color:#FFDC00;
}


.side-nav > li > ul > li > a {
    color: #E6E6E6;
}

.navbar-inverse .navbar-nav > li > a:focus, .navbar-inverse .navbar-nav > li > a:hover {
    color: #E6E6E6;
}

.table-striped > tbody > tr:hover {
 background-color: #FFDC00;   
}


/* Tooltip */
.tooltip-inner {
    background-color:#01FF70;
    color: #333;
    border: 1px solid #333;
} 


  </style>

  </head>  
  <body class="skin-blue">
    <div class="wrapper" id="wrapper">
    

