<!-- Bagian sidebar -->
<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->library('session');
?>
<style type="text/css">

    blink1 {
    -webkit-animation: 1s linear infinite condemned_blink_effect; // for android
    animation: 1s linear infinite condemned_blink_effect;
}
@-webkit-keyframes condemned_blink_effect { // for android
    0% {
        visibility: hidden;
    }
    50% {
        visibility: hidden;
    }
    100% {
        visibility: visible;
    }
}
@keyframes condemned_blink_effect {
    0% {
        visibility: hidden;
    }
    50% {
        visibility: hidden;
    }
    100% {
        visibility: visible;
    }
}

</style>
<script type="text/javascript">

$(document).ready(function() {
    
    
    
});
</script>
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav" >
        <li>
            
            <table style="margin: 10px;">
                <tr>
                    <td >
                        <img src="<?php echo base_url();?>assets/img/photo2.jpg" style="border-radius:50%; width: 60px; height: 60px;border: solid #f0ad4e 2px;" >
                    </td>
                    <td>
                    <table style="margin: 10px;">
                        <tr>
                            <!-- <td style="color:#DDDDDD;">&nbsp;<label style="background-color: #0000006b;">Nama / User</label></td> -->
                        </tr>
                        <tr>
                            <td style="color:#DDDDDD;">
                                <label >
                                
                                <?php 
                                    if (isset($_SESSION["ses_keterangan"]))
                                    {
                                        echo str_replace('\' ', '\'', 
                                            ucwords(str_replace('\'', '\' ', 
                                                strtolower($_SESSION["ses_namauser"])
                                                )
                                            )
                                        );
                                    } 
                                    else 
                                    {
                                        redirect(base_url().'index.php/login',$data);

                                    }
                                ?>
                                
                                </label>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
               
                <tr></tr>
                <tr></tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <form action="<?php echo base_url(); ?>index.php/login/datalogin" method="get">
                            <input type="submit" value="LOGOUT" class="btn btn-warning btn-sm"  
                                 name="LOGOUT" id="LOGOUT"/></input>
                        </form>
                    </td>
                </tr>
            </table>
            </form>
        </li>
         <?php 
          
            $uri =& load_class('URI', 'core');
            $this->load->library('session');
            $this->load->database();
            //echo $this->session->userdata("ses_sidebar");
        ?>
        <!-- <li id="masterbed" style="background-color:rgba(0, 0, 0, 0.4);">
            <a href='javascript:;' data-toggle='collapse' data-target='#colmasterbed'>
                <i class='fa fa-fw fa-lg fa-bed'></i> Master Bed <span class='pull-right'>
                    <i class='fa fa-fw fa-caret-down'>
                    </i>
                </span>
            </a>
            <ul id="colmasterbed" class="collapse" style="font-size: 12px;">
                <li><a href='<?php echo base_url(); ?>index.php/masterbed/masterbedview'><i class='fa fa-fw fa-th'></i> Master Bed </a></li>
              
            </ul>
        </li> -->


         <li >
            <a href="<?php echo base_url(); ?>index.php/monitoringstok/monitoringstokview"><i class="fa fa-lg fa-fw fa-table"></i> Monitoring Stok </a>
        </li>
        <?php 
          
            $uri =& load_class('URI', 'core');
            $this->load->library('session');
            $this->load->database();
            $level = $this->session->userdata('ses_leveluser');
            if ($level<2)
            {
                $base=base_url();
             echo '
                <li>
                    <a href="'.$base.'index.php/settinguser/settinguserview"><i class="fa fa-lg fa-user-circle"></i> Setting User </a>
                </li>';
            }
            //echo $this->session->userdata("ses_sidebar");
        ?>
        
        


        <li id="entridata" >
            <a href='javascript:;' data-toggle='collapse' data-target='#colentridata'>
                <i class='fa fa-fw fa-lg fa-paper-plane'></i> Entri Data <span class='pull-right'>
                    <i class='fa fa-fw fa-caret-down'>
                    </i>
                </span>
            </a>
            <ul id="colentridata" class="collapse" style="font-size: 12px;">
                <li><a href='<?php echo base_url(); ?>index.php/barangmasuk/barangmasuklist'><i class='fa fa-fw fa-th'></i> Barang Masuk </a></li>
                <li><a href='<?php echo base_url(); ?>index.php/barangkeluar/barangkeluarlist'><i class='fa fa-fw fa-th'></i> Barang Keluar </a></li>
            </ul>
            
        </li>
       <!--  <li id="editdata">
            <a href='javascript:;' data-toggle='collapse' data-target='#coleditdata'>
                <i class='fa fa-fw fa-lg fa-pencil-square-o '></i> Edit Data <span class='pull-right'>
                    <i class='fa fa-fw fa-caret-down'>
                    </i>
                </span>
            </a>
            <ul id="coleditdata" class="collapse" style="font-size: 12px;">
                <li><a href='<?php echo base_url(); ?>index.php/editbarangmasuk/editbarangmasukview'><i class='fa fa-fw fa-th'></i> Edit Barang Masuk </a></li>
                <li><a href='<?php echo base_url(); ?>index.php/editbarangkeluar/editbarangkeluarview'><i class='fa fa-fw fa-th'></i> Edit Barang Keluar </a></li>
            </ul>
        </li> -->
        <li id="laporan">
            <a href='javascript:;' data-toggle='collapse' data-target='#collaporan'>
                <i class='fa fa-fw fa-lg fa-file-pdf-o '></i> Laporan <span class='pull-right'>
                    <i class='fa fa-fw fa-caret-down'>
                    </i>
                </span>
            </a>
            <ul id="collaporan" class="collapse" style="font-size: 12px;">
                <li><a href='<?php echo base_url(); ?>index.php/laporandonasi/laporandonasiview'><i class='fa fa-fw fa-th'></i> Laporan Donasi </a></li>
            </ul>
            
        </li>
                
 
</div>
<!-- /.navbar-collapse -->
</nav>

<style>
.navbar-inverse .navbar-nav > li > a{
    color: white;
    padding-top: 5px;
    padding-bottom: 5px;
}

.parent{
 position: relative;
 width: 400px;
 overflow: hidden;
}
 
.child{
 width: 418px;
 overflow-y: scroll;
}
</style>


