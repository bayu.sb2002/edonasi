<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Halaman Tidak Ditemukan!
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
	
	<h2 style="margin-left:8%;color:#d32132"><i class="fa fa-exclamation-triangle fa-3x"></i></h2>

	<div style="margin-left:2%;" >
	<h4>Apa Yang Harus Dilakukan ? </h4>
	<p>1. <a href="javascript:window.history.go(-1);">Kembali Ke Halaman Sebelumnya</a></p>
	<p>2. Minta Ijin Ke Administrator</p>
	<p>3. Hubungi IT Support Untuk Panduan Lebih Lanjut</p>
	</div>
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->