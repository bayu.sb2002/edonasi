<?php

function is_logged_in() {
    // Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    $logged_in = $CI->session->userdata('logged_in');
    if (!isset($logged_in)) 
    { 
    	return false; 
    } 
    else 
    { 
    	return true; 
    }
}


?>