<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('encryption');
?>

<!-- javascript untuk format Rupiah -->
<style type="text/css">
	.warnaaktif{
	 background-image: linear-gradient(to right, #7cf2b5, #7cf2b5, #7cf2b5, #7cf2b5, #7cf2b5);
	}
	.warnabatal{
	background-image: linear-gradient(to right, #de6c85, #de6c85, #de6c85, #de6c85, #de6c85);
	}
</style>

<script>
	function simpanlog(){
	   jQuery.ajax({
	         type: "POST",
	         url: "simpanlog", // the method we are calling
	         dataType: 'text',
	         success: function (data) {

	         		window.open("<?php echo base_url(); ?>"+"index.php/laporandonasi/reportdonasi/","_blank");
	         },
	         error: function (xhr,status,error) {
	             swal(xhr.responseText);
	         }
		});
	}
	function batalcetak(Xurutan){
		swal({
		  title: "Apakah anda yakin?",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
		  	jQuery.ajax({
				type: "POST",
				url: "batalcetak",
				data: {
	       	 		urutan: Xurutan 
	       	 	},
				dataType: 'text',
				success: function (data) {
					swal("Berhasil di batalcetak", {
				      icon: "success",
				    });
				    tabellog();
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			
			});
		    
		  } 
		});
	 	
	}	
	function tabellog(){
		
		$('#tabelList1').DataTable({
			"destroy": true,
			"processing" : true,
	        "ajax": {
		            "url": "tabellog",
		            "type": "POST",
		            "data": {
	       	 				}
			        },
	        "columns"       : [
	        	{"data" : "kosong"},
	        	{"data" : "no"},
	        	{"data" : "tanggal"},
	        	{"data" : "nama"},
	        	{"data" : "batalcetak"},
	        	{"data" : "userbatal"}

            ],
           	"lengthChange": false,
	        "ordering": false,
	        "createdRow"    : function ( row, data, index ) {
               if (data['active']==1)
               {
               		$('td', row).addClass('warnaaktif');	
	           }
	           if (data['active']==0)
               {
               		$('td', row).addClass('warnabatal');	
	           }
            },
            responsive: {
		              details: {
		                  type: 'column',
		                  target: 'tr'
		              }
		          },
		          columnDefs: [{
			            className: 'control',
			            orderable: false,
			            width: 20,
			            targets: 0
			            }
					],
					/*"dom": 'Bfrtip',
				    "buttons": [
				    	{
				            extend: 'excel',
				            text: 'Export to Excel',
				            className: 'exportexcel',
				            title: 'eDonasi - Data Barang Keluar'
		        		}
				    ],*/
				  "scrollY": 200,
                  "scrollX": true,
		          scrollCollapse : true,
		          language: {
		            sSearch      : '<span> Cari Data </span> _INPUT_',
		            lengthMenu   : 'Tampilkan data  _MENU_  baris',
		            info         : 'Menampilkan halaman _PAGE_ dari total _PAGES_ halaman',
		            infoEmpty    : 'Tidak ada data yang ditampilkan',
		            infoFiltered : '(Dicari dari total _MAX_ data)',
		            zeroRecords  : 'Maaf tidak ada hasil manapun dari data yang dicari',
		            oPaginate: {
		                sNext     : 'Berikutnya',
		                sPrevious : 'Sebelumnya',
		                sLast     : 'Halaman Terakhir',
		                sFirst    : 'Halaman Awal'

		            }
		          }
	    }); 
	    

	    
	}
    $(document).ready(function() {
	   $("#btngenerate").click(function(e){
	   		swal({
		  title: "Apakah anda yakin ingin cetak laporan ?",
		  icon: "info",
		  buttons: true,
		  dangerMode: true,
		})
		.then((will) => {
		  if (will) {
		  	simpanlog();
	   		 tabellog();
		    
		  } 
		});
	   		

	   });
	   tabellog();
	});
	
	
</script>

<div id="page-wrapper">          
	<h1>
		Laporan Donasi
	</h1>
	<ol class="breadcrumb">
		<li><i class="fa fa-home"></i> Laporan </li>
		<li> Laporan Donasi </li>
		
	</ol>

	<!-- Main content -->
	<section class="content">
		<div class="modal-body" > 
			
			<div class="row">
				
				<div class="col-md-12">
				 		<button class="btn btn-primary" id="btngenerate">Generate Laporan</button>

				</div>
				<div class="col-md-12">
					<h4 align="center">
						<label>RIWAYAT CETAK</label>
					</h4>

					<table id="tabelList1" class="table table-striped table-bordered" cellspacing="0" width="100%">
		                  <thead class="bg-info">
		                    <tr>
		                      <th></th>  
		                      <th class="all">No</th>                                
		                      <th class="all">Tanggal Cetak</th>
		                      <th class="all">Nama</th>
		                      <th class="all">Batal Cetak</th>
		                      <th class="none">User Batal</th>
		                   	</tr>
		                  </thead>

		                  <tbody>
		                    
		                  
		                  </tbody>
		            </table>
				</div>
			 </div>
		</div>
	</section><!-- /.content -->
	
</div><!-- /#page-wrapper -->

</body>

</html>

