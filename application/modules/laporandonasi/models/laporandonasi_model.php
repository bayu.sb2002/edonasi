<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class laporandonasi_model extends CI_Model{

  /*private $db2;*/
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
      //$this->db2 = $this->load->database('sirs', TRUE);

  }
 
 
  public function get_datalist()
  {
    $this->load->database();
    $sql = "exec pengadaan..don_laporandonasi_datalist";
    return $this->db->query($sql)->result(); 
  }
  public function set_logprint($user,$ip)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_laporandonasi_logprint '$user','$ip'";
    return $this->db->query($sql)->result(); 
  }
  public function get_datalog()
  {
    $this->load->database();
    $sql = "exec pengadaan..don_laporandonasi_datalog ";
    return $this->db->query($sql)->result(); 
  }
  public function set_logbatalcetak($user,$urutan)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_laporandonasi_simpanbatalcetak '$user','$urutan' ";
    return $this->db->query($sql)->result(); 
  }
  
  
  
    
}
