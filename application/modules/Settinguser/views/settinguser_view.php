<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('encryption');
?>

<!-- javascript untuk format Rupiah -->


<script>
	var tampungnomorID;
	tampungnomorID='';

    $(document).ready(function() {
	   	
		$("#idlevel").select2({
	 	 placeholder: "Pilih",
	  	allowClear: true
		});
	   	tabeluser();
	  	datamasterlevel();
	  	$("#btnsimpanubah").click(function(){
	  		simpanubahlevel();
			
		});
	});
	function tabeluser(){
		$('#tabelList1').DataTable( {
			"destroy": true,
			"processing" : true,
	        "ajax": 'tabeluser',
	        "columns"       : [
                {"data" : "namauser"},
                {"data" : "namalevel"},
                {"data" : "ubah"}
                

            ],
            "createdRow"    : function ( row, data, index ) {
                $('td', row).eq(0).addClass('text-center');
               // $('td', row).eq(2).addClass('text-right');
            }, 
	    } );
	}
	function datamasterlevel(){
		
			jQuery.ajax({
				type: "POST",
				url: "datamasterlevel",
				dataType: 'text',
				success: function (data) {
					$('#idlevel').html('');
					$('#idlevel').val(null).trigger('change');
					$('#idlevel').html(data);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			
			});
	}
	function ubahlevel(XnomorID){
		tampungnomorID='';
		tampungnomorID=XnomorID;
		$('#modalubahlevel').modal('show');
	}
	function nonaktif(XnomorID){
		tampungnomorID='';
		tampungnomorID=XnomorID;
	}
	
	function simpanubahlevel(){
		swal({
		  title: "Perhatian",
		  text: "Apakah Yakin Ingin Mengubah ?",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((willDelete) => {
		  if (willDelete) {
		  	jQuery.ajax({
		         type: "POST",
		         url: "simpanubahlevel", // the method we are calling
		         //contentType: "application/json; charset=utf-8",
		       	 data: {
		       	 		nomorID: tampungnomorID,
		       	 		level: $('#idlevel').val()
		       	 		},
		         dataType: 'json',
		         success: function (data) {
           			if (data['ALERT']=='')
           			{
           				swal("Berhasil Diubah", {
					      icon: "success",
					    });	
					    tabeluser();
					    datamasterlevel();
					    $('#modalubahlevel').modal('hide');

           			}
		            else
		            {
		            	swal(data['ALERT']);
		            }
		           
		         },
		         error: function (xhr,status,error) {
		             alert(xhr.responseText);
		         }
			});
		   
		  } 
		});
		
	}
	
	
	
</script>

<div id="page-wrapper">          
	<h1>
		Setting User
	</h1>
	<ol class="breadcrumb">
		<li><i class="fa fa-home"></i> User </li>
		<li> Setting </li>
		
	</ol>

	<!-- Main content -->
	<section class="content">
		<div class="modal-body" > 
			<div id="callout-navbar-btn-context " class="bs-callout bs-callout-info">
				
				<br>
				
				<table id="tabelList1" class="table table-striped table-bordered nowrap " cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Nama User</th>	
							<th>Level</th>
							<th>Ubah</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
				<div class="modal fade" id="modalubahlevel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      
				      <div class="modal-body">
				      	<div id="callout-navbar-btn-context " class="bs-callout bs-callout-info">
					      	<div class="row">
					      		<label>Level</label>
					       		<select name="idlevel" id="idlevel" style="width: 100%" class="  form-control " aria-label="..."  >
								</select>
					       	</div>
				       	</div>
				      </div>
				      <div class="modal-footer">
				        <button name="btnsimpanubah" id="btnsimpanubah" type="button" class="btn btn-primary">Simpan</button>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
				      </div>
				    </div>
				  </div>
				</div>

										
			</div>
		</div>
	</section><!-- /.content -->
	
</div><!-- /#page-wrapper -->

</body>

</html>

