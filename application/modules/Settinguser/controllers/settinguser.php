<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class settinguser extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('settinguser_model');

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    $this->load->view('body_header');
	    $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect('index.php/settinguser/settinguserview', $data);
	}

	public function settinguserview()
	{
		//---------------------------------------------------------------
		//load library
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		
		$this->_render('settinguser_view');
		
	}

	public function tabeluser()
	{
		$query=$this->settinguser_model->get_datalistuser();
		
		$data 			= array();
		foreach($query->result() as $kolom){
			$btn='<button type="button" id="btnbatal" class="cari btn btn-info pull-left" onclick="ubahlevel('.$kolom->ID_USER.')">Ubah Level</button>';
			$data[] 	= array(
				"namauser" 		=> $kolom->NAMAUSER,
				"namalevel" 	=> $kolom->NAMALEVEL,
				"ubah"		=> $btn
				
			);
		}

		$obj 			= array(
			"data" 			=> $data
		);
		
		echo json_encode($obj);
	}
	

	public function simpanubahlevel()
	{
		//---------------------------------------------------------------
		//load library
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->model('settinguser_model');
		
		
		$nomorID      = $this->input->post('nomorID');
		$level      = $this->input->post('level');
		$user = $this->session->userdata('ses_keterangan');		
		$query = $this->settinguser_model->set_simpanubahlevel($nomorID,$user,$level);
		
		$data['ALERT']=$query->row()->ALERT;

		echo json_encode($data);
		
	}
	public function datamasterlevel()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
	
			
			$query=$this->settinguser_model->get_datamasterlevel();
			$opt="<option value=''></option>";
			foreach($query->result() as $data){
				$opt 		.= "<option value='".$data->KDLEVEL."'>".$data->NAMA."</option>";
			}

			echo $opt;
	}
	


}
