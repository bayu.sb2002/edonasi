<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barangmasuk_model extends CI_Model{

  /*private $db2;*/
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
      //$this->db2 = $this->load->database('sirs', TRUE);

  }
  


  public function get_list($tanggalAwal, $tanggalAkhir)
  {
    $this->load->database();
    $tanggalAwal = $this->db->escape((new DateTime($tanggalAwal))->format('Y-m-d'));
    $tanggalAkhir = $this->db->escape((new DateTime($tanggalAkhir))->format('Y-m-d'));
    
    $sql = "EXEC pengadaan.dbo.don_barangmasuk_barangmasuklist_getlist ".$tanggalAwal.",".$tanggalAkhir;
    return $this->db->query($sql)->result();  
  }


  public function get_barang($parameterCari)
  {
      $this->load->database();
      $parameterCari = $this->db->escape($parameterCari);
    $sql = "EXEC pengadaan.dbo.don_barangmasuk_barangmasuk_getbarang ".$parameterCari;
    return $this->db->query($sql);
  }


  public function get_pendonasi($parameterCari)
  {
      $this->load->database();
      $parameterCari = $this->db->escape($parameterCari);
    $sql = "EXEC pengadaan.dbo.don_barangmasuk_barangmasuk_getpendonasi ".$parameterCari;
    return $this->db->query($sql);
  }


  public function get_jumlah()
  {
    $this->load->database();
    $sql = "EXEC pengadaan.dbo.don_barangmasuk_barangmasuk_getjumlah ";
    return $this->db->query($sql)->result(); 
  }
  public function get_datakategori()
  {
    $this->load->database();
    $sql = "select ID_KATEGORI, NAMA_KATEGORI, CRTDT, CRTUSR from pengadaan..DON_KATEGORI";
    return $this->db->query($sql)->result(); 
  }
  public function get_datasatuan()
  {
    $this->load->database();
    $sql = "select ID_SATUAN, NAMA_SATUAN, CRTDT, CRTUSR from pengadaan..DON_SATUAN";
    return $this->db->query($sql)->result(); 
  }
  public function get_datajenisdonasi()
  {
    $this->load->database();
    $sql = "select ID_JENISDONASI,NAMA_JENISDONASI,CRTDT,CRTUSR from pengadaan..DON_JENISDONASI";
    return $this->db->query($sql)->result(); 
  }
  public function get_datasubjenisdonasi($idjenisdonasi)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_barangmasuk_datasubjenisdonasi '$idjenisdonasi'";
    return $this->db->query($sql)->result(); 
  }
  public function get_datanamapenerima($param)
  {
    $this->load->database();
    $sql = "select ID_NAMAPENERIMA, NAMA_PENERIMA, AKTIF, CRTDT, CRTUSR, MODIDT, MODIUSR from pengadaan..DON_PENERIMA_BARANGMASUK where NAMA_PENERIMA like '%$param%' and aktif=1";
    return $this->db->query($sql)->result(); 
  }
  public function get_ceksatuanbarang($namabarang)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_barangmasuk_ceksatuanbarang '$namabarang'";
    return $this->db->query($sql)->result(); 
  }
   public function set_bataltransaksi($urutan,$user)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_barangmasuk_bataltransaksi '$urutan','$user'";
    return $this->db->query($sql); 
  }
  
  
  

  public function save_data($barang, $tanggal, $pendonasi, $keterangan, $jumlah, $contact,$pengiriman,$kategoribarang, $satuan, $namapenerima,$jenisdonasi,$subjenisdonasi, $user) 
  {
    $this->load->database();
    $barang = $this->db->escape($barang); 
    $tanggal = $this->db->escape((new DateTime($tanggal))->format('Y-m-d')); 
    $pendonasi = $this->db->escape($pendonasi); 
    $keterangan = $this->db->escape($keterangan); 
    $jumlah = $this->db->escape($jumlah);
    $contact = $this->db->escape($contact);
    $pengiriman = $this->db->escape($pengiriman);
    $kategoribarang = $this->db->escape($kategoribarang);
    $satuan = $this->db->escape($satuan);
    $namapenerima = $this->db->escape($namapenerima);
    $user = $this->db->escape($user);
    $jenisdonasi="'".$jenisdonasi."'";
    $subjenisdonasi="'".$subjenisdonasi."'";
    
    $sql = "EXEC pengadaan.dbo.don_barangmasuk_barangmasuksimpan ".
    $barang.",". 
    $tanggal.",". 
    $pendonasi.",". 
    $keterangan.",". 
    $jumlah.",".
    $contact.",". 
    $pengiriman.",".
    $kategoribarang.",". 
    $satuan.",". 
    $namapenerima.",". 
    $jenisdonasi.",". 
    $subjenisdonasi.",". 
    $user;
    return $this->db->query($sql); 

  }
 
    
}
