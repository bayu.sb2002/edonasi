<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barangmasuk extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('Barangmasuk_model');

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    $this->load->view('body_header');
	    $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect('index.php/barangmasuk/barangmasuklist', $data);
	}

	public function barangmasuklist()
	{
		//---------------------------------------------------------------
		//load library
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		
		$this->_render('barangmasuk_list');
		//$this->load->view('barangmasuk_list');	
		
	}



   public function getbarang()
   {
   	   $uri =& load_class('URI', 'core');
		$this->load->model('Barangmasuk_model');
		$this->load->library('session');
		$this->load->library('encryption');
		// redirect jika tidak login
			
		// Get the third segment
		$id = $uri->segment(3); 
		$id = $this->encryption->decrypt(str_replace(array('-', '_', '~'), array('+', '/', '='), $id));
		if($id != "edonasi"){
			$this->_render('no_access');
		}
		else
		{
			if(isset($_GET['term']))
			{
				$parameterCari = $_GET['term'];
			}
			else
			{
				$parameterCari = "";	
			}

			
			// error catch
			if($this->Barangmasuk_model->get_barang($parameterCari))
			{
				$query = $this->Barangmasuk_model->get_barang($parameterCari)->result();

				//var_dump( $query);
				header('Content-Type: application/json');
				echo json_encode($query);		
			}
			else
			{
				header('Content-Type: application/json');
				echo '{
					    "sEcho": 1,
					    "iTotalRecords": "0",
					    "iTotalDisplayRecords": "0",
					    "aaData": []
					}';
			}

		}
   }


   public function barangmasuksimpan()
   {
   	//---------------------------------------------------------------
		//load library
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Barangmasuk_model');
		// redirect jika tidak login
        
        $barang = strtoupper($this->input->post('txtBarang'));
        $tanggal = $this->input->post('txtTanggal');
        $pendonasi = strtoupper($this->input->post('txtPendonasi'));
        $keterangan = strtoupper($this->input->post('txtKeterangan'));
        $jumlah = $this->input->post('txtJumlah');
        $contact = strtoupper($this->input->post('txtContact'));
		$pengiriman = strtoupper($this->input->post('txtPengiriman'));
		$kategoribarang = strtoupper($this->input->post('txtKategoribarang'));
		$satuan = strtoupper($this->input->post('txtSatuan'));
		$namapenerima = strtoupper($this->input->post('txtNamapenerima'));
		$jenisdonasi = $this->input->post('txtJenisdonasi');
		if ($jenisdonasi=='')
		{
			$jenisdonasi='0';
		}
		$subjenisdonasi = $this->input->post('txtSubjenisdonasi');
		if ($subjenisdonasi=='')
		{
			$subjenisdonasi='0';
		}
        $user = $this->session->userdata('ses_keterangan');


        if($barang != "" AND $tanggal != "" AND $pendonasi != "" AND $jumlah != "" AND $pengiriman != "" AND $kategoribarang != "" AND $satuan != "")
        {
        	$hasilExe 	  = $this->Barangmasuk_model->save_data($barang, $tanggal, $pendonasi, $keterangan, $jumlah, $contact,$pengiriman,$kategoribarang, $satuan, $namapenerima,$jenisdonasi,$subjenisdonasi, $user);	

        	echo json_encode(array("INFO"=>"BERHASIL" ));
        }
        else
        {
        	echo json_encode(array("INFO"=>"GAGAL" ));
        }
	
   }



    public function getpendonasi()
   {
   	   $uri =& load_class('URI', 'core');
		$this->load->model('Barangmasuk_model');
		$this->load->library('session');
		$this->load->library('encryption');
		// redirect jika tidak login
			
		// Get the third segment
		$id = $uri->segment(3); 
		$id = $this->encryption->decrypt(str_replace(array('-', '_', '~'), array('+', '/', '='), $id));
		if($id != "edonasi"){
			$this->_render('no_access');
		}
		else
		{
			if(isset($_GET['term']))
			{
				$parameterCari = $_GET['term'];
			}
			else
			{
				$parameterCari = "";	
			}

			
			// error catch
			if($this->Barangmasuk_model->get_pendonasi($parameterCari))
			{
				$query = $this->Barangmasuk_model->get_pendonasi($parameterCari)->result();

				//var_dump( $query);
				header('Content-Type: application/json');
				echo json_encode($query);		
			}
			else
			{
				header('Content-Type: application/json');
				echo '{
					    "sEcho": 1,
					    "iTotalRecords": "0",
					    "iTotalDisplayRecords": "0",
					    "aaData": []
					}';
			}

		}
   }


   public function getjumlah()
   {

        $uri =& load_class('URI', 'core');
      $this->load->model('Barangmasuk_model');
      $this->load->library('session');
      $this->load->library('encryption');
      // redirect jika tidak login

        // Get the third segment
        $id = $uri->segment(3);     
        $id = $this->encryption->decrypt(str_replace(array('-', '_', '~'), array('+', '/', '='), $id));
        if($id != "edonasi"){
          $this->_render('no_access');
        }
        else
        {

          
          $tanggalAwal = $this->input->post('tanggalAwal');
          $tanggalAkhir = $this->input->post('tanggalAkhir');
    
          // error catch
          if($this->Barangmasuk_model->get_jumlah())
          {
            $query = $this->Barangmasuk_model->get_jumlah();
            foreach ($query as &$object)
            {
               //$object->A = (new DateTime($object->A))->format('d-m-Y');
               //$object->E = (new DateTime($object->E))->format('d-m-Y');
            }

            //var_dump( $query);
            header('Content-Type: application/json');
            echo json_encode($query);    
          }
          else
          {
            header('Content-Type: application/json');
            echo '{
                  "sEcho": 1,
                  "iTotalRecords": "0",
                  "iTotalDisplayRecords": "0",
                  "aaData": []
              }';
          }

        }
   }


	public function getlist()
	{
		$uri =& load_class('URI', 'core');
		$this->load->model('Barangmasuk_model');
		$this->load->library('session');
		$this->load->library('encryption');
		// redirect jika tidak login
				
		// Get the third segment
		$id = $uri->segment(3); 
		$id = $this->encryption->decrypt(str_replace(array('-', '_', '~'), array('+', '/', '='), $id));
		if($id != "edonasi"){
			$this->_render('no_access');
		}
		else
		{


			$tanggalAwal = $uri->segment(4); 
			$tanggalAkhir = $uri->segment(5); 


			// error catch
			if($this->Barangmasuk_model->get_list($tanggalAwal, $tanggalAkhir))
			{
				$query = $this->Barangmasuk_model->get_list($tanggalAwal, $tanggalAkhir);

				$counter = 0;
				foreach ($query as &$object)
				{
				    $object->M = (new DateTime($object->M))->format('d-m-Y');
				    $object->O = (new DateTime($object->O))->format('d-m-Y');
				    if ($object->Q==1)
				    { 
				    $object->Q = '<button type="button" id="btnbatal" class="cari btn btn-danger pull-left" onclick="bataltransaksi('.$object->B.')">Batal</button>';
				   	}
				   	else
				   	{
				   		$object->Q='Sudah Cetak';
				   	}
				   	if ($object->R==0)
				   	{
				   		$object->Q='BATAL';
				   	}
					/*$object->I = number_format(round($object->I),0);
					$object->K = number_format(round($object->K),0);
					$object->C = (new DateTime($object->C))->format('d-m-Y');
					*/


				}

				//var_dump( $query);
				header('Content-Type: application/json');
				echo json_encode(array('data' => $query));		
			}
			else
			{
				header('Content-Type: application/json');
				echo '{
					    "sEcho": 1,
					    "iTotalRecords": "0",
					    "iTotalDisplayRecords": "0",
					    "aaData": []
					}';
			}
		}		
	}
	public function datakategori()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangmasuk_model');


			$parameter1       = $this->input->post('parameter1');
			$query=$this->barangmasuk_model->get_datakategori($parameter1);
			$data 			= array();
			foreach($query as $hasil){
				$data[] 	= array(
					"id" 	=> $hasil->NAMA_KATEGORI,
					"text" 	=> $hasil->NAMA_KATEGORI
					
				);
			}
						
			echo json_encode($data);
			
	}
	public function datasatuan()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangmasuk_model');

			/*$parameter1       = $this->input->post('parameter1');
			$query=$this->barangmasuk_model->get_datasatuan($parameter1);
			$data 			= array();
			foreach($query as $hasil){
				$data[] 	= array(
					"id" 	=> $hasil->NAMA_SATUAN,
					"text" 	=> $hasil->NAMA_SATUAN
					
				);
			}
						
			echo json_encode($data);*/

			$query=$this->barangmasuk_model->get_datasatuan();
			$opt="<option value=''></option>";
			foreach($query as $data){
				$opt 		.= "<option value='".$data->NAMA_SATUAN."'>".$data->NAMA_SATUAN."</option>";
			}

			echo $opt;
		
			
	}
	public function datajenisdonasi()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangmasuk_model');

			$query=$this->barangmasuk_model->get_datajenisdonasi();
			$opt="<option value=''></option>";
			foreach($query as $data){
				$opt 		.= "<option value='".$data->ID_JENISDONASI."'>".$data->NAMA_JENISDONASI."</option>";
			}

			echo $opt;
			
	}

	public function datasubjenisdonasi()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangmasuk_model');

			$idjenisdonasi       = $this->input->post('idjenisdonasi');
			$query=$this->barangmasuk_model->get_datasubjenisdonasi($idjenisdonasi);
			$opt="<option value=''></option>";
			foreach($query as $data){
				$opt 		.= "<option value='".$data->ID_DET_JENISDONASI."'>".$data->NAMA_JENISDONASI_DET."</option>";
			}

			echo $opt;
			
	}
	public function datanamapenerima()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangmasuk_model');

			$parameter1       = $this->input->post('parameter1');
			$query=$this->barangmasuk_model->get_datanamapenerima($parameter1);
			$data 			= array();
			foreach($query as $hasil){
				$data[] 	= array(
					"id" 	=> $hasil->NAMA_PENERIMA,
					"text" 	=> $hasil->NAMA_PENERIMA
					
				);
			}
						
			echo json_encode($data);
			
	}
	public function ceksatuanbarang()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangmasuk_model');

			$namabarang       = strtoupper($this->input->post('namabarang'));
			$query=$this->barangmasuk_model->get_ceksatuanbarang($namabarang);

			$opt="<option value=''></option>";
			$data = array();
			foreach($query as $row){
				if ($row->PILIH==1)
				{
					$opt        ="<option disabled value=''></option>";
					$opt 		.= "<option selected value='".$row->NAMA_SATUAN."'>".$row->NAMA_SATUAN."</option>";
					$data['option']=$opt;
					$data['selected']=$row->NAMA_SATUAN;

				}
				else if ($row->PILIH==0)
				{
					$data['option']='';
					$data['selected']='';
				}
			}

			echo json_encode($data);
			
	}
	public function bataltransaksi()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangmasuk_model');


			$urutan       = $this->input->post('urutan');
			$user = $this->session->userdata('ses_keterangan');

			$query=$this->barangmasuk_model->set_bataltransaksi($urutan,$user);
			
			$data 			= array();
			$data['ALERT']=$query->row()->ALERT;
			echo json_encode($data);
			
	}
	


	


}
