<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('encryption');
$url = str_replace(array('+', '/', '='), array('-', '_', '~'), $this->encryption->encrypt('edonasi'));
?>

  <!-- Datepicker -->
   <!--  <link rel="stylesheet" href="<?php echo base_url();?>assets/datepicker/bootstrap-datetimepicker.css">
    <script src="<?php echo base_url();?>assets/datepicker/moment-with-locales.js"></script>
    <script src="<?php echo base_url();?>assets/datepicker/bootstrap-datetimepicker.js"></script> -->
<style type="text/css">
	.panel-body{
		font-size: 24px;
		font-weight: 700;
	}

	.panel-heading{
		font-weight: 700;
	}
	.select2-search__field{
		text-transform:uppercase;
	}
	.select2-results__option{
		text-transform:uppercase;
	}
	.select2-selection__rendered{
		text-transform:uppercase;
	}
	.form-control{
		text-transform:uppercase;
	}
	.warnaaktif{
	 background-image: linear-gradient(to right, #7cf2b5, #7cf2b5, #7cf2b5, #7cf2b5, #7cf2b5);
	}
	.warnabatal{
	background-image: linear-gradient(to right, #de6c85, #de6c85, #de6c85, #de6c85, #de6c85);
	}
	
</style>

<script>

    $(document).ready(function() {

    	$("#txtJumlah").autoNumeric('init',{vMin:'0',aSign:'',aSep:'',aPad: false,mDec:'2',aDec:'.',aForm: false});

    	$('#txtTanggalAwal').datetimepicker({
        format: 'DD-MM-YYYY',
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
        }
   		});



    	$('#txtTanggalAkhir').datetimepicker({
        format: 'DD-MM-YYYY',
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
        }
    	});


    	$('#txtTanggal').datetimepicker({
        format: 'DD-MM-YYYY',
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
        }
    	});


    	$("#btnPerbaharui").click(function(){
		  reloadTabel();
		}); 
		$('#txtJenisdonasi').change(function(event){
			
    	  if ($(this).val()=='1')
    	  {
    	  	$('#txtSubjenisdonasi').addClass('fields1');
    	  	$('#txtSubjenisdonasi').removeAttr('disabled',true);
    	  	datasubjenisdonasi();
    	  }
    	  else
    	  {
    	  	$('#txtSubjenisdonasi').removeClass('fields1');
    	  	$('#txtSubjenisdonasi').val(null).trigger('change');	
    	  	$('#txtSubjenisdonasi').attr('disabled',true);

    	  }
   		});
   		$('#txtBarang').change(function(event){
			ceksatuanbarang();

   		});
    	$("#txtPengiriman").select2({
	 	 placeholder: "Pilih",
	  	allowClear: true
		});
		$("#txtKondisibarang").select2({
	 	 placeholder: "Pilih",
	  	allowClear: true
		});
		$("#txtSatuan").select2({
	 	 placeholder: "Pilih",
	  	allowClear: true
		});
		$("#txtJenisdonasi").select2({
	 	 placeholder: "Pilih",
	  	allowClear: true
		});
		$("#txtSubjenisdonasi").select2({
	 	 placeholder: "Pilih",
	  	allowClear: true
		});

		$("#btnsimpan").click(function(){
			cekfields();
		});     
    	
      
       /*$("#txtKategoribarang").select2({
		   		minimumInputLength: 0,
			   	tags: true,
				allowClear: true,
				maximumSelection:1,
				placeholder: "Ketik nama satuan",
			    ajax: {
			      	type: "POST",
		         	url: "datakategori", // the method we are calling
		         	dataType: 'json',
		        
				    data: function (params) {
				        return {
				          parameter1: params.term, // search term
				          page: params.page
				        };
				      },
				    processResults: function (data) {
				              return {
				                  results: $.map(data, function(obj) {
				                      return { id: obj.id, text: obj.text};
				                  })
				              };
				          },
				      cache: false
			    }
		});*/
       $("#txtKategoribarang").select2({
		   		minimumInputLength: 0,
			   	tags: true,
				allowClear: true,
				maximumSelection:1,
				placeholder: "Ketik Kategori",
			    ajax: {
			      	type: "POST",
		         	url: "datakategori", // the method we are calling
		         	dataType: 'json',
		            data: function (params) {
				        return {
				          parameter1: params.term, // search term
				          page: params.page
				        };
				      },
				    processResults: function (data) {
				              return {
				                  results: $.map(data, function(obj) {
				                      return { id: obj.id, text: obj.text};
				                  })
				              };
				          },
				     cache: false
			    }
		});
       $("#txtNamapenerima").select2({
		   		minimumInputLength: 0,
			   	tags: true,
				allowClear: true,
				maximumSelection:1,
				placeholder: "Ketik Nama",
			    ajax: {
			      	type: "POST",
		         	url: "datanamapenerima", // the method we are calling
		         	dataType: 'json',
		            data: function (params) {
				        return {
				          parameter1: params.term, // search term
				          page: params.page
				        };
				      },
				    processResults: function (data) {
				              return {
				                  results: $.map(data, function(obj) {
				                      return { id: obj.id, text: obj.text};
				                  })
				              };
				          },
				     cache: false
			    }
		});
       
        reloadJumlah();
        
		reloadPendonasi();
		datasatuan();
		datajenisdonasi();
		reloadBarang();
		reloadTabel();

		   

	});

	function reloadBarang()
    {
      

        $("#txtBarang").select2({
          placeholder: "Ketik Nama Barang",
          tags                   : true,
          width                  : 'resolve',
          allowClear             : true,
          ajax: {
            url: '<?php echo base_url(); ?>barangmasuk/getbarang/<?php echo $url; ?>',
            type : 'GET',
            data: function (term) {
              return term
            },
            dataType: 'json',
            delay: 250,
             processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.A, text: obj.B };
                    })
                };
            },
            cache: true,
            success: function(data){ 
            },
            error: function(xhr, status, error) {
            }
          }
        });
    }

	function reloadJumlah()
       {
          var tanggalAwal  = $('#txtTanggalAwal').val();
          var tanggalAkhir = $('#txtTanggalAkhir').val();

           $("#spanHari").text(0);
           $("#spanMinggu").text(0);
           $("#spanBulan").text(0);
           $("#spanKeseluruhan").text(0); 

          $.ajax({
               url: '<?php echo base_url()."barangmasuk/getjumlah/".$url;?>',
               type: 'POST',
               data: {
                 tanggalAwal  : tanggalAwal,
                 tanggalAkhir : tanggalAkhir
               },
                 dataType: "json",  
               success: function(data){
                var json = data;

                 
                if (typeof json[0] !== 'undefined') {
                  $("#spanHari").text(json[0].HARI);
                  $("#spanMinggu").text(json[0].MINGGU);
                  $("#spanBulan").text(json[0].BULAN);
                  $("#spanKeseluruhan").text(json[0].KESELURUHAN);

                }
                else
                {
                  $("#spanHari").text(0);
                  $("#spanMinggu").text(0);
                  $("#spanBulan").text(0);
                  $("#spanKeseluruhan").text(0); 
                }
            },
            error: function(xhr, status, error) {
               //alert(xhr.responseText);
            }
           });
       }
	function reloadTabel()
    {
    
      tanggalAwal = $('#txtTanggalAwal').val();	
      tanggalAkhir = $('#txtTanggalAkhir').val();
      
      var tableList =  $('#tabelListMasuk').DataTable({
          sAjaxSource: '<?php echo base_url(); ?>barangmasuk/getlist/<?php echo $url; ?>/'+tanggalAwal+'/'+tanggalAkhir,
          deferRender: true,
          columns: [
                { data: 'A' },
                { data: 'Q' },
                { data: 'B' },
                { data: 'C' },
                { data: 'D' },
                { data: 'E' },
                { data: 'F' },
                { data: 'G' },
                { data: 'H' },
                { data: 'I' },
                { data: 'J' },
                { data: 'K' },
                { data: 'L' },
                { data: 'M' },
                { data: 'N' },
                { data: 'O' },
                { data: 'P' }

            ],
            responsive: {
              details: {
                  type: 'column',
                  target: 'tr'
              }
          },
          /*columnDefs: [ {
              //className: 'control',
              orderable: false,
              targets:   0,
              visible: false
          } ],*/
          columnDefs: [{
	            className: 'control',
	            orderable: false,
	            width: 20,
	            targets: 0
	            }
			],
		  "dom": 'Bfrtip',
		    "buttons": [
		    	{
		            extend: 'excel',
		            text: 'Export to Excel',
		            className: 'exportexcel',
		            title: 'eDonasi - Data Barang Masuk',
		            exportOptions: {
		                columns: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
		            }
        		}
		    ],
		     "createdRow"    : function ( row, data, index ) {
               if (data['Q']=='BATAL')
               {
               		$('td', row).addClass('warnabatal');	
	           }
	          
            },
		  "scrollY": 200,
          "scrollX": true,
          scrollCollapse : true,
          paging         : true,
          bInfo          : false,
          bFilter        : true,
          processing     : true,
          destroy        : true,
          language: {
            sSearch      : '<span> Cari Data </span> _INPUT_',
            lengthMenu   : 'Tampilkan data  _MENU_  baris',
            info         : 'Menampilkan halaman _PAGE_ dari total _PAGES_ halaman',
            infoEmpty    : 'Tidak ada data yang ditampilkan',
            infoFiltered : '(Dicari dari total _MAX_ data)',
            zeroRecords  : 'Maaf tidak ada hasil manapun dari data yang dicari',
            oPaginate: {
                sNext     : 'Berikutnya',
                sPrevious : 'Sebelumnya',
                sLast     : 'Halaman Terakhir',
                sFirst    : 'Halaman Awal'

            }
          },
          order: []
         
      });
  }
	function cekfields(){
		var jml 	= $(".fields1").length;
		var cek 	= 0;
		$('.fields1').each(function(i,e){
		    if($(e).val() != ""){
		        cek++;
			}

	 	});
	 	if(jml == cek){
	 		simpandata();
		}else{
	 		swal("ADA ISIAN YANG KOSONG");
	 	}
	}
	function reloadPendonasi()
    {
        $("#txtPendonasi").select2({
          placeholder: "Ketik Pendonasi",
          tags                   : true,
          width                  : 'resolve',
          allowClear             : true,
          maximumSelection:1,
          ajax: {
            url: '<?php echo base_url(); ?>barangmasuk/getpendonasi/<?php echo $url; ?>',
            type : 'GET',
            data: function (term) {
              return term
            },
            dataType: 'json',
            delay: 250,
             processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.A, text: obj.B };
                    })
                };
            },
            cache: true,
            success: function(data){ 
            },
            error: function(xhr, status, error) {
            }
          }
        });
        
    }
    function datasatuan(){
		
			jQuery.ajax({
				type: "POST",
				url: "datasatuan",
				dataType: 'text',
				success: function (data) {
					$('#txtSatuan').val(null).trigger('change');
					$('#txtSatuan').html(data);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			
			});
			/* $("#txtSatuan").select2({
		   		//tags: true,
		   		minimumInputLength: 1,
				allowClear: true,
				placeholder: "Ketik nama satuan",
			    ajax: {
			      	type: "POST",
		         	url: "datasatuan", // the method we are calling
		         	dataType: 'json',
		        
				    data: function (params) {
				        return {
				          parameter1: params.term, // search term
				          page: params.page
				        };
				      },
				    processResults: function (data) {
				              return {
				                  results: $.map(data, function(obj) {
				                      return { id: obj.id, text: obj.text};
				                  })
				              };
				          },
				      cache: false
			    }
		});*/
	}
	function datajenisdonasi(){
		
			jQuery.ajax({
				type: "POST",
				url: "datajenisdonasi",
				dataType: 'text',
				success: function (data) {
					$('#txtJenisdonasi').html(data);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			
			});
			
	}
	function datasubjenisdonasi(){
		
			jQuery.ajax({
				type: "POST",
				url: "datasubjenisdonasi",
				data: {
	       	 		idjenisdonasi: $('#txtJenisdonasi').val()
	       	 	},
				dataType: 'text',
				success: function (data) {
					$('#txtSubjenisdonasi').html(data);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			
			});
			
	}
	function ceksatuanbarang(){
		
			jQuery.ajax({
				type: "POST",
				url: "ceksatuanbarang",
				data: {
	       	 		namabarang: $('#txtBarang').val()
	       	 	},
				dataType: 'json',
				success: function (data) {
					if (data['option']!='')
					{
						$('#txtSatuan').html(data['option']);
						$('#txtSatuan').val(data['selected']); // Select the option with a value of '1'
						$('#txtSatuan').trigger('change'); // Notify any JS components that the value changed
					}
					else
					{
						datasatuan();
					}
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			
			});
			
	}
	function simpandata(){
			swal({
				  title: "Apakah anda yakin?",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				})
				.then((willDelete) => {
				  if (willDelete) {
				    	$.ajax({
					         url: '<?php echo base_url()."barangmasuk/barangmasuksimpan";?>',
					         type: 'POST',
					         //data: $("#formInput").serialize(),
					         data: {
					            txtTanggal    : $('#txtTanggal').val(),
					            txtBarang     : $('#txtBarang').val(),
					            txtPendonasi  : $('#txtPendonasi').val(),
					            txtJumlah     : $('#txtJumlah').val(),
					            txtKeterangan : $('#txtKeterangan').val(),
					            txtContact    : $('#txtContact').val(),
					            txtPengiriman : $('#txtPengiriman').val(),
					            txtKategoribarang : $('#txtKategoribarang').val(),
					            txtSatuan 	  : $('#txtSatuan').val(),
					            txtNamapenerima	  : $('#txtNamapenerima').val(),
					            txtJenisdonasi	  : $('#txtJenisdonasi').val(),
					            txtSubjenisdonasi : $('#txtSubjenisdonasi').val()
					         },
					           dataType: "json",  
					         success: function(data){
					             var json = data

					             if(json.INFO == "BERHASIL")
					             {
					             	reloadTabel();
						             $("#txtJumlah").val(0);
						             $("#txtKeterangan").val('');
						             $("#txtTanggal").val('<?php echo date("d-m-Y") ?>');
						             
						            /* reloadBarang();
						             reloadPendonasi();
						             reloadJumlah();*/
						             /*$("#txtBarang").select2("val", "");
						             $("#txtPendonasi").select2("val", "");	
						             $("#txtPengiriman").select2("val", "");	*/
						             $("#txtBarang").val(null).trigger("change");
						             $("#txtPendonasi").val(null).trigger("change");
						             $("#txtPengiriman").val(null).trigger("change");
						             $("#txtContact").val("");	
						             $("#txtKategoribarang").val(null).trigger("change");
						             $("#txtSatuan").val(null).trigger("change");	
						             $("#txtNamapenerima").val(null).trigger("change");	
						             $("#txtJenisdonasi").val(null).trigger("change");	
						             $("#txtSubjenisdonasi").val(null).trigger("change");	
						             $("#txtKategoribarang").val(null).trigger("change");
						             $("#txtNamapenerima").val(null).trigger("change");	
						             

					             }
					             else
					             {
					             	alert("Gagal simpan, harap lengkapi data terlebih dahulu !");
					             }

					             
					         },

					         error: function(xhr, status, error) {
					         alert(xhr.responseText);
					      }
					     });		
				  } 
				});

	}
	function bataltransaksi(Xurutan){
			swal({
				  title: "Apakah anda yakin?",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				})
				.then((willDelete) => {
				  if (willDelete) {
				    	$.ajax({
					         //url: '<?php echo base_url()."barangmasuk/barangmasuksimpan";?>',
					         url: 'bataltransaksi',
					         type: 'POST',
					         //data: $("#formInput").serialize(),
					         data: {
					            urutan    : Xurutan
					            
					         },
							 dataType: "json",  
					         success: function(data){
					             

					             if(data['ALERT'] =='')
					             {
					             	swal('Berhasil Dibatalkan');
					             	reloadTabel();
					             }
					             else
					             {
					             	swal(data['ALERT']);
					             }

					             
					         },

					         error: function(xhr, status, error) {
					         alert(xhr.responseText);
					      }
					     });		
				  } 
				});

	}
	
</script>

<div id="page-wrapper">          
	<h1>
		Entri Barang Masuk
	</h1>
	<ol class="breadcrumb">
		<li><i class="fa fa-home"></i> Entri </li>
		<li> Barang Masuk </li>
		
	</ol>

	<!-- Main content -->
	<section class="content">
		<div class="modal-body" > 
			
			<div class="row">
				
				<div class="col-md-12 text-center">
					<span style="font-style: 700; font-size: 22px;">Resume Donasi Masuk</span>
				</div>
				<div class="col-md-3">
					<div class="panel panel-info text-center">
					  <div class="panel-heading">Hari Ini</div>
					  <div class="panel-body">
					    <span id="spanHari">0</span>
					  </div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="panel panel-info text-center">
					  <div class="panel-heading">Minggu Ini</div>
					  <div class="panel-body">
					    <span id="spanMinggu">0</span>
					  </div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="panel panel-info text-center">
					  <div class="panel-heading">Bulan Ini</div>
					  <div class="panel-body">
					    <span id="spanBulan">0</span>
					  </div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="panel panel-info text-center">
					  <div class="panel-heading">Keseluruhan</div>
					  <div class="panel-body">
					    <span id="spanKeseluruhan">0</span>
					  </div>
					</div>
				</div>
			</div>
			<div id="callout-navbar-btn-context " class="bs-callout bs-callout-info">
			
				<div class="row">
					
					<div class="col-md-12">
						<div class="form-group">
							<label>Nama Pendonasi</label>
							<div>
							<select required id="txtPendonasi" style="text-transform: uppercase" class="form-control fields1">
								<option disabled selected value=""></option>
							</select>	
							</div>
						</div>
						
					</div>
					
					
					<div class="col-md-6">
						<div class="form-group">
							<label>Jenis Donasi</label>
							<select required id="txtJenisdonasi" style="text-transform: uppercase" class="form-control fields1">
								<!-- <option disabled selected value=""></option> -->
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Sub Jenis Donasi</label>
							<select required id="txtSubjenisdonasi" style="text-transform: uppercase" class="form-control" disabled="">
								<!-- <option disabled selected value=""></option> -->
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label> Jenis Pengiriman </label>
							<select required id="txtPengiriman" class="form-control fields1">
								<option >  </option>
								<option value="KURIR"> Kurir </option>
								<option value="LANGSUNG TERIMA"> Langsung Terima </option>
							</select>
						</div>	
					</div>
					<div class="col-md-6">
						<div class="form-group">
						  <label>Tanggal Terima</label>
						  <div class="input-group date" >
			                  <input required type="text" class="form-control fields1" placeholder="Tanggal" name="txtTanggal" id="txtTanggal"  value="<?php echo date('d-m-Y'); ?>" />
			                  <span class="input-group-addon">
			                      <span class="glyphicon glyphicon-calendar"></span>
			                  </span>
			              </div>
			            </div>  
					</div>
					<div class="col-md-12">
						<div class="form-group">
						  <label>Kategori Barang</label>
						  <select required id="txtKategoribarang" class="form-control fields1">
						  	<option disabled selected value=""></option>
						  </select>
			            </div> 
			        </div> 
					<div class="col-md-4">
					    <div class="form-group">
							<label>Nama Barang</label>
							<select required id="txtBarang" class="form-control fields1" >
								<option disabled selected value=""></option>
							</select>
						</div>	
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Satuan</label>
							<select required id="txtSatuan" class="form-control fields1">
								<option disabled selected value=""></option>
								<!-- <option disabled selected value=""></option> -->
							</select>
						</div>	
						
					</div>
					<div class="col-md-4">
						
						<div class="form-group">
							<label>Jumlah</label>
							<input required type="text" name="txtJumlah" id="txtJumlah" class="form-control fields1" >
						</div>
					</div>
					<!-- <div class="col-md-12">
						<div class="form-group">
							<label>Kondisi Barang</label>
							<select required id="txtKondisibarang" class="form-control">
								<option></option>
								<option value="1">Layak</option>
								<option value="2">Tidak Layak</option>
							</select>
						</div>
					</div> -->
					<div class="col-md-6">
						<div class="form-group">
							<label>Nama Penerima</label>
							<select id="txtNamapenerima" class="form-control">
								<option disabled selected value=""></option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Keterangan</label>
							<input type="text" name="txtKeterangan" id="txtKeterangan" class="form-control" >
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Contact Pendonasi / Telp</label>
							<input required type="text" name="txtContact" id="txtContact" class="form-control " >
						</div>
					</div>
					<div class="col-md-12">
						<br>
						<button class="btn btn-primary" id="btnsimpan">Simpan Data</button>
						<br><br>
						
					</div>	



					<div class="col-md-4">
					
			              <div class="input-group date" >
			                  <input type="text" class="form-control" placeholder="Tanggal" name="txtTanggalAwal" id="txtTanggalAwal" value="<?php echo date('d-m-Y'); ?>" />
			                  <span class="input-group-addon">
			                      <span class="glyphicon glyphicon-calendar"></span>
			                  </span>
			              </div>
					</div>
					<div class="col-md-4">
						
			              <div class="input-group date" >
			                  <input type="text" class="form-control" placeholder="Tanggal" name="txtTanggalAkhir" id="txtTanggalAkhir" value="<?php echo date('d-m-Y'); ?>" />
			                  <span class="input-group-addon">
			                      <span class="glyphicon glyphicon-calendar"></span>
			                  </span>
			              </div>
					</div>
					<div class="col-md-4">
						<button class="btn btn-primary" id="btnPerbaharui">Perbaharui Data</button>
					</div>
					<br>
					<div class="col-md-12" style="border-top-style: solid;border-top-width: 0px;margin-top: 20px;">
						 <table id="tabelListMasuk" class="table table-striped table-bordered" cellspacing="0" width="100%">
			                  <thead class="bg-info">
			                    <tr>
			                      <th></th>  
			                      <th>Batal</th>  
			                      <th>ID</th>                                
			                      <th>Nama Barang</th>
			                      <th>Nama Kategori</th>
			                      <th>Jumlah</th>
			                      <th>Satuan</th>
			                      <th>Pengiriman</th>
			                      <th>User Input</th>
			                      <th>Nama Pendonasi</th>
			                      <th>Jenis Donasi</th>
			                      <th>Sub Jenis Donasi</th>
			                      <th>Nama Penerima</th>
			                      <th>Tanggal Terima</th>
			                      <th>Keterangan</th>
			                      <th>Tanggal Input</th>
			                      <th>Contact</th>
			                    </tr>
			                  </thead>

			                  <tbody>
			                    
			                  
			                </tbody>
			              </table>
					</div>
					
					
				</div>							
			</div>
		</div>
	</section><!-- /.content -->
	
</div><!-- /#page-wrapper -->

</body>

</html>

