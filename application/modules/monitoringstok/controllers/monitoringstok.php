<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class monitoringstok extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('monitoringstok_model');

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    $this->load->view('body_header');
	    $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect('index.php/monitoringstok/monitoringstokview', $data);
	}

	public function monitoringstokview()
	{
		//---------------------------------------------------------------
		//load library
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		
		$this->_render('monitoringstok_view');
		
	}

	
	
	public function datajenisdonasi()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('monitoringstok_model');

			$query=$this->monitoringstok_model->get_datajenisdonasi();
			$opt="<option value=''></option>";
			foreach($query as $data){
				$opt 		.= "<option value='".$data->ID_JENISDONASI."' value2='".$data->NAMA_JENISDONASI."'>".$data->NAMA_JENISDONASI."</option>";
			}

			echo $opt;
			
	}

	public function datasubjenisdonasi()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('monitoringstok_model');

			$idjenisdonasi       = $this->input->post('idjenisdonasi');
			$query=$this->monitoringstok_model->get_datasubjenisdonasi($idjenisdonasi);
			$opt="<option value=''></option>";
			foreach($query as $data){
				$opt 		.= "<option value='".$data->ID_DET_JENISDONASI."' value2='".$data->NAMA_JENISDONASI_DET."'>".$data->NAMA_JENISDONASI_DET."</option>";
			}

			echo $opt;
			
	}
	public function datakategori()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('monitoringstok_model');


			$parameter1       = $this->input->post('parameter1');
			$query=$this->monitoringstok_model->get_datakategori($parameter1);
			$data 			= array();
			foreach($query as $hasil){
				$data[] 	= array(
					"id" 	=> $hasil->ID_KATEGORI,
					"text" 	=> $hasil->NAMA_KATEGORI
					
				);
			}
						
			echo json_encode($data);
			
	}
	public function tabelstokbarang()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('monitoringstok_model');
		

		$idjenisdonasi   = $this->input->post('idjenisdonasi');
		$idsubjenisdonasi   = $this->input->post('idsubjenisdonasi');
		$idkategoribarang   = $this->input->post('idkategoribarang');
		
		$query=$this->monitoringstok_model->get_datastokbarang($idjenisdonasi,$idsubjenisdonasi,$idkategoribarang);
		
		$data 			= array();
		foreach($query as $kolom){
			$data[] 	= array(
				"kosong" 		=> "",
				"idbarang" 		=> $kolom->ID_MBARANG,
				"namabarang" 	=> $kolom->NAMA_BARANG,
				"stokawal" 		=> $kolom->STOKAWAL,
				"stokkeluar" 	=> $kolom->STOKKELUAR,
				"stokakhir" 	=> $kolom->STOKAKHIR,
				"satuan"		=> $kolom->SATUAN

			);
		}

		$obj 			= array(
			"data" 			=> $data
		);
		
		echo json_encode($obj);
	}

}
