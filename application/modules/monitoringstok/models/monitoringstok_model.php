<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class monitoringstok_model extends CI_Model{

  /*private $db2;*/
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
      //$this->db2 = $this->load->database('sirs', TRUE);

  }
 
  public function get_datajenisdonasi()
  {
    $this->load->database();
    $sql = "select ID_JENISDONASI,NAMA_JENISDONASI,CRTDT,CRTUSR from pengadaan..DON_JENISDONASI";
    return $this->db->query($sql)->result(); 
  }
  public function get_datasubjenisdonasi($idjenisdonasi)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_barangmasuk_datasubjenisdonasi '$idjenisdonasi'";
    return $this->db->query($sql)->result(); 
  }
  public function get_datakategori($param)
  {
    $this->load->database();
    $sql = "select ID_KATEGORI, NAMA_KATEGORI, CRTDT, CRTUSR from pengadaan..DON_KATEGORI where NAMA_KATEGORI like '%$param%'";
    return $this->db->query($sql)->result(); 
  }
  public function get_datastokbarang($idjenisdonasi,$idsubjenisdonasi,$idkategoribarang)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_monitoringstok_databarang '$idjenisdonasi','$idsubjenisdonasi','$idkategoribarang'";
    return $this->db->query($sql)->result(); 
  }
  
    
}
