<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('encryption');
?>

<!-- javascript untuk format Rupiah -->


<script>
	var namakategori;
    $(document).ready(function() {

	    $("#txtJenisdonasi").select2({
		 	 placeholder: "Pilih",
		  	allowClear: false
		});
		$("#txtSubjenisdonasi").select2({
		 	 placeholder: "Pilih",
		  	allowClear: false
		});
		$("#txtKategoribarang").select2({
		 	 placeholder: "Pilih",
		  	allowClear: false
		});
		$('#txtJenisdonasi').change(function(event){
			
    	  if ($(this).val()=='1')
    	  {
    	  	$('#txtSubjenisdonasi').addClass('fields1');
    	  	$('#txtSubjenisdonasi').removeAttr('disabled',true);
    	  	datasubjenisdonasi();
    	  }
    	  else
    	  {
    	  	$('#txtSubjenisdonasi').removeClass('fields1');
    	  	$('#txtSubjenisdonasi').val(null).trigger('change');	
    	  	$('#txtSubjenisdonasi').attr('disabled',true);

    	  }
   		});
   		$('#txtKategoribarang').on('select2:select', function (e) {
    		var data = e.params.data;
    		namakategori=data['text'];
    	});
   		datajenisdonasi();
   		datakategori();
   		$('#txtKategoribarang').change(function(event){
	   		tabelstokbarang();
	   	
		});
		$('#txtJenisdonasi').change(function(event){
	   		tabelstokbarang();
	   	
		});
		$('#txtSubjenisdonasi').change(function(event){
	   		tabelstokbarang();
	   	
		});
		$('#btnreload').click(function(event){
			tabelstokbarang();
		});
	});
	function datajenisdonasi(){
		
			jQuery.ajax({
				type: "POST",
				url: "datajenisdonasi",
				dataType: 'text',
				success: function (data) {
					$('#txtJenisdonasi').html(data);
					
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			
			});
			
	}
	function datasubjenisdonasi(){
		
			jQuery.ajax({
				type: "POST",
				url: "datasubjenisdonasi",
				data: {
	       	 		idjenisdonasi: $('#txtJenisdonasi').val()
	       	 	},
				dataType: 'text',
				success: function (data) {
					$('#txtSubjenisdonasi').html(data);
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			
			});
			
	}
	function datakategori(){
		$("#txtKategoribarang").select2({
		   		minimumInputLength: 0,
			   	tags: true,
				allowClear: false,
				maximumSelection:1,
				placeholder: "Ketik Kategori",
			    ajax: {
			      	type: "POST",
		         	url: "datakategori", // the method we are calling
		         	dataType: 'json',
		            data: function (params) {
				        return {
				          parameter1: params.term, // search term
				          page: params.page
				        };
				      },
				    processResults: function (data) {
				              return {
				                  results: $.map(data, function(obj) {
				                      return { id: obj.id, text: obj.text};
				                  })
				              };
				          },
				     cache: false
			    }
		});
	}
	function tabelstokbarang(){
		
		var namafile;
		namafile = $('#txtJenisdonasi :selected').attr('value2')+'-'+namakategori;
		if ($('#txtSubjenisdonasi :selected').attr('value2')=='')
		{
			namafile = $('#txtJenisdonasi :selected').attr('value2')+'-'+$('#txtSubjenisdonasi :selected').attr('value2')+'-'+namakategori;
		}

		if ($('#txtJenisdonasi').val()=='' || $('#txtKategoribarang').val()==null || $('#txtKategoribarang').val()=='')
		{
			$('#tabelList1').DataTable();
		}
		else
		{
			if ($('#txtJenisdonasi').val()=='1' && $('#txtSubjenisdonasi').val()=='')
			{
				$('#tabelList1').DataTable();		
			}
			else
			{
				$('#tabelList1').DataTable({
					"destroy": true,
					"processing" : true,
			        "ajax": {
				            "url": "tabelstokbarang",
				            "type": "POST",
				            "data": {
			       	 				idjenisdonasi : $('#txtJenisdonasi').val(),
			       	 				idsubjenisdonasi : $('#txtSubjenisdonasi').val(),
			       	 				idkategoribarang : $('#txtKategoribarang').val()
			       	 				}
					        },
			        "columns"       : [
			        	{"data" : "kosong"},
			        	{"data" : "idbarang"},
			        	{"data" : "namabarang"},
			        	{"data" : "stokawal"},
			        	{"data" : "stokkeluar"},
			        	{"data" : "stokakhir"},
			        	{"data" : "satuan"}
		            ],
		           	"lengthChange": false,
			        "ordering": false,
			        "createdRow"    : function ( row, data, index ) {
		               
		            },
		            responsive: {
				              details: {
				                  type: 'column',
				                  target: 'tr'
				              }
				          },
				          /*columnDefs: [ {
				              //className: 'control',
				              orderable: false,
				              targets:   0,
				              visible: false
				          } ],*/
				          columnDefs: [{
					            className: 'control',
					            orderable: false,
					            width: 20,
					            targets: 0
					            }
							],
							"dom": 'Bfrtip',
						    "buttons": [
						    	{
						            extend: 'excel',
						            text: 'Export to Excel',
						            className: 'exportexcel',
						            title: 'eDonasi-StokBarang-'+namafile
				        		}
						    ],
						  "scrollY": 200,
		                  "scrollX": true,
				          scrollCollapse : true,
				          language: {
				            sSearch      : '<span> Cari Data </span> _INPUT_',
				            lengthMenu   : 'Tampilkan data  _MENU_  baris',
				            info         : 'Menampilkan halaman _PAGE_ dari total _PAGES_ halaman',
				            infoEmpty    : 'Tidak ada data yang ditampilkan',
				            infoFiltered : '(Dicari dari total _MAX_ data)',
				            zeroRecords  : 'Maaf tidak ada hasil manapun dari data yang dicari',
				            oPaginate: {
				                sNext     : 'Berikutnya',
				                sPrevious : 'Sebelumnya',
				                sLast     : 'Halaman Terakhir',
				                sFirst    : 'Halaman Awal'

				            }
				          }
			    }); 
			}
		}
	    

	    
	}
	
	
	
	
</script>

<div id="page-wrapper">          
	<h1>
		Monitoring Stok
	</h1>
	<ol class="breadcrumb">
		<li><i class="fa fa-home"></i> Monitoring Stok </li>
		
		
	</ol>

	<!-- Main content -->
	<section class="content">
		<div class="modal-body" > 
			<div id="callout-navbar-btn-context " class="bs-callout bs-callout-info">
				<div class="row">
					<div class="col-md-12">
						<div class="form-inline">
							<div class="form-group">
									<label>Jenis Donasi</label>
									<select required id="txtJenisdonasi" style="text-transform: uppercase; width: 100%" class="form-control fields1">
										<option disabled selected value=""></option>
									</select>
							</div>
							<div class="form-group">
									<label>Sub Jenis Donasi</label>
									<select required id="txtSubjenisdonasi" style="text-transform: uppercase; width: 100%" class="form-control" disabled="">
										<option disabled selected value=""></option>
									</select>
							</div>
							<div class="form-group">
								  <label>Kategori Barang</label>
								  <select required id="txtKategoribarang" style="width: 100%" class="form-control">
								  	<option disabled selected value=""></option>
								  </select>
					        </div> 

					        
						</div>
						<br>
						<div >
						 <button style="width: 100%" name="btnreload" id="btnreload" type="button" class="btn btn-info">Refresh</button>
						</div>
					</div>

				</div>
				
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      
				      <div class="modal-body">
				      	<div id="callout-navbar-btn-context " class="bs-callout bs-callout-info">
					      	<div class="row">
					      		<label>Password</label>
					       		<input name="pass" id="pass" type="password" class="form-control" style="text-transform:uppercase" placeholder="Masukan Password" aria-label="..." required>
					       	</div>
				       	</div>
				      </div>
				      <div class="modal-footer">
				        <button name="btnpass" id="btnpass" type="button" class="btn btn-primary">Simpan</button>
				        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
				      </div>
				    </div>
				  </div>
				</div>

				<br>
				
				<table id="tabelList1" class="table table-striped table-bordered nowrap " cellspacing="0" width="100%">
					<thead>
						<tr>
							<th></th>
							<th width="10%">ID</th>
							<th>Nama Barang</th>	
							<th>Stok Awal</th>
							<th>Stok Keluar</th>
							<th>Stok Akhir</th>
							<th>Satuan</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
										
			</div>
		</div>
	</section><!-- /.content -->
	
</div><!-- /#page-wrapper -->

</body>

</html>

