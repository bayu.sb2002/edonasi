<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporandonasi extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('laporandonasi_model');

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    $this->load->view('body_header');
	    $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect('index.php/laporandonasi/laporandonasiview', $data);
	}

	public function laporandonasiview()
	{
		//---------------------------------------------------------------
		//load library
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		
		$this->_render('laporandonasi_view');
		
	}
	function get_client_ip() {
	    $ipaddress = '';
		    if (isset($_SERVER['HTTP_CLIENT_IP']))
		        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		    else if(isset($_SERVER['HTTP_X_FORWARDED']))
		        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		    else if(isset($_SERVER['HTTP_FORWARDED']))
		        $ipaddress = $_SERVER['HTTP_FORWARDED'];
		    else if(isset($_SERVER['REMOTE_ADDR']))
		        $ipaddress = $_SERVER['REMOTE_ADDR'];
		    else
		        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}
	public function simpanlog() 
	{	
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('laporandonasi_model');

		$ip=$this->get_client_ip();
		$user = $this->session->userdata('ses_keterangan');
		$this->laporandonasi_model->set_logprint($user,$ip);
		
	}
	
	public function reportdonasi() 
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('laporandonasi_model');

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('Laporan Donasi');
		$pdf->setPrintHeader(false);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		
		//$pdf->AddPage();
		$pdf->AddPage('L', 'A4');
		if (isset($_SESSION['ses_keterangan']))
		{
			$html='<table border="0" >
						<tr bgcolor="#ffffff" >
							<td width="16%" >
										<label></label>
										<img id="foto" width="110" src="image/Logo-RSPHC.png">
								
							</td>';

			$html.='		<td width="84%">
								<table border="0" >
									<tr>
										<td>
										<font size="9">
										RUMAH SAKIT PHC SURABAYA
										</font>
										</td>
										<td>
										</td>
									</tr>	
									<tr>
										<td>
										<font size="9">	
										Jl. Prapat kurung Selatan NO. 1 Tanjung Perak Surabaya 60165
										</font>
										</td>
										<td>
										</td>
									</tr>	
									<tr>
										<td style="width:80%;">	
										<font size="9">
										Telp. (031)3294801-3 FAX. (031)3294804 UGD 24JAM (031)3294118
										</font>
										</td>
										<td style="width:20%;" align="right">
										<div style="font-weight: bold;"></div>
										</td>
									</tr>

								</table>
							</td>
						</tr>
					</table>
					<hr style="margin-bottom: 0px;margin-top: 0px;">
					';
			$html.=	'
					<h3 align="center">LAPORAN DONASI COVID-19 PT PELINDO HUSADA CITRA</h3>';
			$html.=	'
					<h5 align="center">PERIODE : '.date('d-m-Y').'</h5>';
			
			$html.='<font size="8">
					<table cellspacing="1"  bgcolor="#000000" cellpadding="1">
						<tr bgcolor="#ffffff" >
							<td width="20px" align="center" rowspan="3"><br><br>NO</td>
							<td width="130px" align="center" rowspan="3"><br><br>NAMA BARANG</td>
							<td width="50px" align="center" rowspan="3"><br><br>SATUAN</td>
							<td width="50px" align="center" rowspan="3"><br><br>KATEGORI</td>
							<td width="204px" align="center" colspan="6">PELINDO</td>
							<td width="102px" align="center" colspan="3" rowspan="2"><br><br>IHC</td>
							<td width="102px" align="center" colspan="3" rowspan="2"><br><br>UMUM</td>
							<td width="102px" align="center" colspan="3" rowspan="2"><br><br>TOTAL</td>
						</tr>
						<tr bgcolor="#ffffff">
							<td width="102px" align="center" colspan="3">PHC</td>
							<td width="102px" align="center" colspan="3">DROP</td>
						</tr>
						<tr bgcolor="#ffffff">
							<td align="center"><b>IN</b></td>
							<td align="center"><b>OUT</b></td>
							<td align="center"><b>SISA</b></td>
							<td align="center"><b>IN</b></td>
							<td align="center"><b>OUT</b></td>
							<td align="center"><b>SISA</b></td>
							<td align="center"><b>IN</b></td>
							<td align="center"><b>OUT</b></td>
							<td align="center"><b>SISA</b></td>
							<td align="center"><b>IN</b></td>
							<td align="center"><b>OUT</b></td>
							<td align="center"><b>SISA</b></td>
							<td align="center"><b>IN</b></td>
							<td align="center"><b>OUT</b></td>
							<td align="center"><b>SISA</b></td>
						</tr>

						';
			$htmlheader1=$html;

			$query = $this->laporandonasi_model->get_datalist();
			
			$in1=0;
			$out1=0;
			$sisa1=0;
			$in2=0;
			$out2=0;
			$sisa2=0;
			$in3=0;
			$out3=0;
			$sisa3=0;
			$in4=0;
			$out4=0;
			$sisa4=0;
			$inall=0;
			$outall=0;
			$sisaall=0;
			$hitungpagebreak=1;
			$i=1;
			$data = array();
			foreach($query as $kolom){
						$in1=$in1+$kolom->IN1;
						$out1=$out1+$kolom->OUT1;
						$sisa1=$sisa1+$kolom->SISA1;
						$in2=$in2+$kolom->IN2;
						$out2=$out2+$kolom->OUT2;
						$sisa2=$sisa2+$kolom->SISA2;
						$in3=$in3+$kolom->IN3;
						$out3=$out3+$kolom->OUT3;
						$sisa3=$sisa3+$kolom->SISA3;
						$in4=$in4+$kolom->IN4;
						$out4=$out4+$kolom->OUT4;
						$sisa4=$sisa4+$kolom->SISA4;
						$inall=$inall+$kolom->INALL;
						$outall=$outall+$kolom->OUTALL;
						$sisaall=$sisaall+$kolom->SISAALL;
						

					//untuk IDX=0 menentukan kamar
						$html.=
							'<tr bgcolor="#ffffff">
								<td width="20px" align="center">'.$i.'</td>
								<td width="130px">'.$kolom->NAMA_BARANG.'</td>
								<td width="50px" align="center">'.$kolom->SATUAN.'</td>
								<td width="50px" align="center">'.$kolom->KATEGORI.'</td>
								<td width="34px" align="center">'.$kolom->IN1.'</td>
								<td width="34px" align="center">'.$kolom->OUT1.'</td>
								<td width="34px" align="center">'.$kolom->SISA1.'</td>
								<td width="34px" align="center">'.$kolom->IN2.'</td>
								<td width="34px" align="center">'.$kolom->OUT2.'</td>
								<td width="34px" align="center">'.$kolom->SISA2.'</td>
								<td width="34px" align="center">'.$kolom->IN3.'</td>
								<td width="34px" align="center">'.$kolom->OUT3.'</td>
								<td width="34px" align="center">'.$kolom->SISA3.'</td>
								<td width="34px" align="center">'.$kolom->IN4.'</td>
								<td width="34px" align="center">'.$kolom->OUT4.'</td>
								<td width="34px" align="center">'.$kolom->SISA4.'</td>
								<td width="34px" align="center">'.$kolom->INALL.'</td>
								<td width="34px" align="center">'.$kolom->OUTALL.'</td>
								<td width="34px" align="center">'.$kolom->SISAALL.'</td>
							</tr>';
					
					if ($hitungpagebreak===25)
					{
						$html.='</table></font>';
						$pdf->writeHTML($html, true, false, true, false, ''); 
						$pdf->AddPage();
						$html=$htmlheader1;
						$hitungpagebreak=0;
					}

					$i++;
					$hitungpagebreak++;


			}
			$html.=
							'<tr bgcolor="#ffffff">
								<td width="250px" align="center" colspan="4"><b>GRAND TOTAL</b></td>
								<td width="34px" align="center"><b>'.$in1.'</b></td>
								<td width="34px" align="center"><b>'.$out1.'</b></td>
								<td width="34px" align="center"><b>'.$sisa1.'</b></td>
								<td width="34px" align="center"><b>'.$in2.'</b></td>
								<td width="34px" align="center"><b>'.$out2.'</b></td>
								<td width="34px" align="center"><b>'.$sisa2.'</b></td>
								<td width="34px" align="center"><b>'.$in3.'</b></td>
								<td width="34px" align="center"><b>'.$out3.'</b></td>
								<td width="34px" align="center"><b>'.$sisa3.'</b></td>
								<td width="34px" align="center"><b>'.$in4.'</b></td>
								<td width="34px" align="center"><b>'.$out4.'</b></td>
								<td width="34px" align="center"><b>'.$sisa4.'</b></td>
								<td width="34px" align="center"><b>'.$inall.'</b></td>
								<td width="34px" align="center"><b>'.$outall.'</b></td>
								<td width="34px" align="center"><b>'.$sisaall.'</b></td>
							</tr>';

			$html.='</table></font>';
			$pdf->writeHTML($html, true, false, true, false, ''); 
		}

		
		$pdf->SetFont('Helvetica', 'BI', 7, '', 'false');
		$pdf->SetXY(10, 190);
		$pdf->Cell(0, 0, 'Print Date:'.date('d-m-Y H:i:s'));


		

		$pdf->Output('laporanDonasi.pdf', 'I');
		
		

	}

}
