<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('encryption');
?>

<!-- javascript untuk format Rupiah -->


<script>
	function simpanlog(){
	   jQuery.ajax({
	         type: "POST",
	         url: "simpanlog", // the method we are calling
	         dataType: 'text',
	         success: function (data) {
	         		window.open("<?php echo base_url(); ?>"+"index.php/laporandonasi/reportdonasi/","_blank");
	         },
	         error: function (xhr,status,error) {
	             swal(xhr.responseText);
	         }
		});
	}
    $(document).ready(function() {
	   $("#btngenerate").click(function(e){
	   		simpanlog();
	   });
	});
	
	
</script>

<div id="page-wrapper">          
	<h1>
		Laporan Donasi
	</h1>
	<ol class="breadcrumb">
		<li><i class="fa fa-home"></i> Laporan </li>
		<li> Laporan Donasi </li>
		
	</ol>

	<!-- Main content -->
	<section class="content">
		<div class="modal-body" > 
			
			<div class="row">
				
				<div class="col-md-12">
				 		<button class="btn btn-primary" id="btngenerate">Generate Laporan</button>
				 </div>
			 </div>
		</div>
	</section><!-- /.content -->
	
</div><!-- /#page-wrapper -->

</body>

</html>

