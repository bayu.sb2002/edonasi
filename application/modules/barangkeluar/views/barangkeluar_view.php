<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('encryption');
?>

<!-- javascript untuk format Rupiah -->
<!-- <script type="text/javascript" src="http://localhost/rawatinap/assets/maskmoney/jquery.maskMoney.min.js"></script> -->
<style type="text/css">
	.select2-search__field{
		text-transform:uppercase;
	}
	.select2-results__option{
		text-transform:uppercase;
	}
	.select2-selection__rendered{
		text-transform:uppercase;
	}
	.form-control{
		text-transform:uppercase;
	}
</style>
<script type="text/javascript">
	
	
	function tabelbarang(){
		
		$('#tabelList1').DataTable({
			"destroy": true,
			"processing" : true,
	        "ajax": {
		            "url": "tabelbarangkeluar",
		            "type": "POST",
		            "data": {
	       	 				tanggalawal : $('#txtTanggalAwal').val(),
	       	 				tanggalakhir : $('#txtTanggalAkhir').val()
	       	 				}
			        },
	        "columns"       : [
	        	{"data" : "kosong"},
	        	{"data" : "btnbatal"},
	        	{"data" : "idkeluar"},
	        	{"data" : "namajenisdonasi"},
	        	{"data" : "subnamajenisdonasi"},
	        	{"data" : "namakategori"},
                {"data" : "namabarang"},
                {"data" : "jumlah"},
                {"data" : "namasatuan"},
                {"data" : "jenispenerima"},
                {"data" : "nama"},
                {"data" : "alamat"},
                {"data" : "ktp"},
                {"data" : "instansi"},
                {"data" : "tanggaldistribusi"},
                {"data" : "kondisibarang"},
                {"data" : "pengiriman"},
                {"data" : "keterangan"},
                {"data" : "tanggalinput"},
                {"data" : "userinput"}

            ],
           	"lengthChange": false,
	        "ordering": false,
	        "createdRow"    : function ( row, data, index ) {
               
            },
            responsive: {
		              details: {
		                  type: 'column',
		                  target: 'tr'
		              }
		          },
		          /*columnDefs: [ {
		              //className: 'control',
		              orderable: false,
		              targets:   0,
		              visible: false
		          } ],*/
		          columnDefs: [{
			            className: 'control',
			            orderable: false,
			            width: 20,
			            targets: 0
			            }
					],
					"dom": 'Bfrtip',
				    "buttons": [
				    	{
				            extend: 'excel',
				            text: 'Export to Excel',
				            className: 'exportexcel',
				            title: 'eDonasi - Data Barang Keluar',
				            exportOptions: {
				                columns: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
				            }
		        		}
				    ],
				  "scrollY": 200,
                  "scrollX": true,
		          scrollCollapse : true,
		          language: {
		            sSearch      : '<span> Cari Data </span> _INPUT_',
		            lengthMenu   : 'Tampilkan data  _MENU_  baris',
		            info         : 'Menampilkan halaman _PAGE_ dari total _PAGES_ halaman',
		            infoEmpty    : 'Tidak ada data yang ditampilkan',
		            infoFiltered : '(Dicari dari total _MAX_ data)',
		            zeroRecords  : 'Maaf tidak ada hasil manapun dari data yang dicari',
		            oPaginate: {
		                sNext     : 'Berikutnya',
		                sPrevious : 'Sebelumnya',
		                sLast     : 'Halaman Terakhir',
		                sFirst    : 'Halaman Awal'

		            }
		          }
	    }); 
	    

	    
	}
  	function simpanbarangkeluar(){

			jQuery.ajax({
		         type: "POST",
		         url: "simpanbarangkeluar", // the method we are calling
		         //contentType: "application/json; charset=utf-8",
		       	 data: {
		       	 		idjenisdonasi : $('#txtJenisdonasi').val(),
		       	 		idsubjenisdonasi : $('#txtSubjenisdonasi').val(),
		       	 		idkategori : $('#txtKategoribarang').val(),
		       	 		namabarang: $('#namabarang').val(),
		       	 		jumlah    : $('#jumlah').autoNumeric("get"),
		       	 		jenispenerima  : $('#jenispenerima').val(),
		       	 		nama      : $('#nama').val(),
		       	 		alamat    : $('#alamat').val(),
		       	 		instansi  : $('#instansi').val(),
		       	 		ktp  	  : $('#ktp').val(),
		       	 		pengiriman: $('#pengiriman').val(),
		       	 		tanggalkeluar   : $('#tanggalkeluar').val(),
		       	 		tanggaldistribusi   : $('#tanggaldistribusi').val(),
		       	 		kondisibarang   : $('#kondisibarang').val(),
		       	 		keterangan: $('#keterangan').val(),
		       	 		satuan   : $('#satuan').val()
		       	 },
		         dataType: 'json',
		         success: function (data) {
		         	if (data['ALERT']=='')
		         	{
			         	$('#namabarang').val(null).trigger('change');
			         	$('#jumlah').val('');
			         	$('#stok').val(0);
			         	$('#satuan').val('');
			         	$('#jenispenerima').val(null).trigger('change');
			         	$('#nama').val('');
			         	$('#alamat').val('');
			         	$('#ktp').val('');
			         	$('#pengiriman').val(null).trigger('change');
			         	$('#tanggal').val('');
			         	$('#keterangan').val('');
			         	$('#txtJenisdonasi').val(null).trigger('change');
			         	$('#kondisibarang').val(null).trigger('change');
			         	$('#instansi').val(null).trigger('change');
			         	$('#txtKategoribarang').val(null).trigger('change');

		         	}
		         	else
		         	{
		         		swal(data['ALERT']);
		         	}
           			tabelbarang();          
		         },
		         error: function (xhr,status,error) {
		             alert(xhr.responseText);
		         }
			});
	}
	

	

	function cekfields(){
		var jml 	= $(".fields1").length;
		var cek 	= 0;
		$('.fields1').each(function(i,e){
		    if($(e).val() != "" && $(e).val() != null){
		        cek++;
			}

	 	});
	 	if(jml == cek){
	 		swal({
			  title: "Apakah anda yakin?",
			  icon: "warning",
			  buttons: true,
			  dangerMode: true,
			})
			.then((willDelete) => {
			  if (willDelete) {
			    	simpanbarangkeluar();		
			  } 
			});
	 		
	 	}else{
	 		swal("ADA ISIAN YANG KOSONG");
	 	}
	}
	function datajenisdonasi(){
		
			jQuery.ajax({
				type: "POST",
				url: "datajenisdonasi",
				dataType: 'text',
				success: function (data) {
					$('#txtJenisdonasi').html(data);
					databarang();
					$('#namabarang').val(null).trigger('change');
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			
			});
			
	}
	function datasubjenisdonasi(){
		
			jQuery.ajax({
				type: "POST",
				url: "datasubjenisdonasi",
				data: {
	       	 		idjenisdonasi: $('#txtJenisdonasi').val()
	       	 	},
				dataType: 'text',
				success: function (data) {
					$('#txtSubjenisdonasi').html(data);
					databarang();
					$('#namabarang').val(null).trigger('change');
				},
				error: function(xhr, status, error) {
					alert(xhr.responseText);
				}
			
			});
			
	}
	function databarang(){
			$('#stok').val('');
			$('#satuan').val('');
			$("#namabarang").select2({
		   		minimumInputLength: 0,
			   	/*tags: true,*/
				allowClear: true,
				maximumSelection:1,
				placeholder: "ketik nama barang",
			    ajax: {
			      	type: "POST",
		         	url: "databarang", // the method we are calling
		         	dataType: 'json',
		        
				    data: function (params) {
				        return {
				          parameter1: params.term, // search term
				          idjenisdonasi: $('#txtJenisdonasi').val(),
		       	 		  idsubjenisdonasi: $('#txtSubjenisdonasi').val(),
		       	 		  idkategoribarang: $('#txtKategoribarang').val(),
				          page: params.page
				        };
				      },
				    processResults: function (data) {
				              return {
				                  results: $.map(data, function(obj) {
				                      return { id: obj.id, text: obj.text,stok: obj.stok,satuan: obj.satuan};
				                  })
				              };
				          },
				      cache: false
			    }
			});
	}
	function datakategori(){
		$("#txtKategoribarang").select2({
		   		minimumInputLength: 0,
			   	tags: true,
				allowClear: true,
				maximumSelection:1,
				placeholder: "Ketik Kategori",
			    ajax: {
			      	type: "POST",
		         	url: "datakategori", // the method we are calling
		         	dataType: 'json',
		            data: function (params) {
				        return {
				          parameter1: params.term, // search term
				          page: params.page
				        };
				      },
				    processResults: function (data) {
				              return {
				                  results: $.map(data, function(obj) {
				                      return { id: obj.id, text: obj.text};
				                  })
				              };
				          },
				     cache: false
			    }
		});
	}
	function datainstansi(){
		$("#instansi").select2({
		   		minimumInputLength: 0,
			   	tags: true,
				allowClear: true,
				maximumSelection:1,
				placeholder: "Ketik Instansi",
			    ajax: {
			      	type: "POST",
		         	url: "datainstansi", // the method we are calling
		         	dataType: 'json',
		            data: function (params) {
				        return {
				          parameter1: params.term, // search term
				          page: params.page
				        };
				      },
				    processResults: function (data) {
				              return {
				                  results: $.map(data, function(obj) {
				                      return { id: obj.id, text: obj.text};
				                  })
				              };
				          },
				     cache: false
			    }
		});
	}
	function bataltransaksi(Xurutan){
			swal({
				  title: "Apakah anda yakin?",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				})
				.then((willDelete) => {
				  if (willDelete) {
				    	$.ajax({
					         //url: '<?php echo base_url()."barangmasuk/barangmasuksimpan";?>',
					         url: 'bataltransaksi',
					         type: 'POST',
					         //data: $("#formInput").serialize(),
					         data: {
					            urutan    : Xurutan
					            
					         },
							 dataType: "json",  
					         success: function(data){
					             

					             if(data['ALERT'] =='')
					             {
					             	swal('Berhasil Dibatalkan');
					             	tabelbarang();
					             }
					             else
					             {
					             	swal(data['ALERT']);
					             }

					             
					         },

					         error: function(xhr, status, error) {
					         alert(xhr.responseText);
					      }
					     });		
				  } 
				});

	}
	$(document).ready(function() {
			datakategori();
			tabelbarang();
			datajenisdonasi();
			databarang();
			datainstansi();
			$('#tanggalkeluar').datetimepicker({
		       format: 'DD-MM-YYYY'
		    });
			$('#tanggaldistribusi').datetimepicker({
		       format: 'DD-MM-YYYY'
		    });
			  $('#txtTanggalAwal').datetimepicker({
		       format: 'DD-MM-YYYY'
		    });
			$('#txtTanggalAkhir').datetimepicker({
		       format: 'DD-MM-YYYY'
		    });
			$("#btnsimpan").click(function(event) {
				cekfields();
			});	
			$("#jenispenerima").select2({
		 	 placeholder: "Pilih",
		  	allowClear: true
			});
			$("#pengiriman").select2({
		 	 placeholder: "Pilih",
		  	allowClear: true
			});
			$("#txtJenisdonasi").select2({
		 	 placeholder: "Pilih",
		  	allowClear: true
			});
			$("#txtSubjenisdonasi").select2({
		 	 placeholder: "Pilih",
		  	allowClear: true
			});
			$("#kondisibarang").select2({
		 	 placeholder: "Pilih",
		  	allowClear: true
			});

			$("#pass").keypress(function(event) {
				var code = event.keyCode;
				if(code==13){ // enter key has code 13 
			      cekpass();
			    }
			});
			$("#jenispenerima").change(function(event) {
				if ($(this).val()=='INTERNAL')
				{
					$("#alamat").attr("disabled",true);
					$("#ktp").attr("disabled",true);
					$("#alamat").removeClass("fields1");
					$("#ktp").removeClass("fields1");

					$('#pengiriman').val('LANGSUNG TERIMA').trigger('change'); 
					
				}
				else if ($(this).val()=='EKSTERNAL')
				{
					$("#alamat").removeAttr("disabled");
					$("#ktp").removeAttr("disabled");
					$("#alamat").addClass("fields1");
					$("#ktp").addClass("fields1");

					$('#pengiriman').val('').trigger('change'); 
					
				}
			});
			
			$("#jumlah").autoNumeric('init',{aSign:'',aSep:'',aPad: false,mDec:'2',aDec:'.',aForm: false});
			
			$('#namabarang').on('select2:select', function (e) {
		    	var data = e.params.data;
		    	$('#satuan').val(data['satuan']);
		    	$('#stok').val(data['stok']);
		    	$("#jumlah").autoNumeric('destroy');
		    	$("#jumlah").autoNumeric('init',{vMin:'0',vMax:data['stok'],aSign:'',aSep:'',aPad: false,mDec:'2',aDec:'.',aForm: false});
		    	$("#jumlah").autoNumeric("set",0);
		    
		   	});
		   	$('#txtJenisdonasi').change(function(event){
			
	    	  if ($(this).val()=='1')
	    	  {
	    	  	$('#txtSubjenisdonasi').addClass('fields1');
	    	  	$('#txtSubjenisdonasi').removeAttr('disabled',true);
	    	  	datasubjenisdonasi();
	    	  }
	    	  else
	    	  {
	    	  	$('#txtSubjenisdonasi').removeClass('fields1');
	    	  	$('#txtSubjenisdonasi').val(null).trigger('change');	
	    	  	$('#txtSubjenisdonasi').attr('disabled',true);

	    	  }
	    	  databarang();
	    	  $('#namabarang').val(null).trigger('change');
	    	});
	   		$('#txtSubjenisdonasi').change(function(event){
	   				databarang();
	   				$('#namabarang').val(null).trigger('change');
			});
			$('#txtKategoribarang').change(function(event){
	   				databarang();
	   				$('#namabarang').val(null).trigger('change');
			});
			$('#btnPerbaharui').click(function(event){
	   				tabelbarang();
			});
			
			
	});
	
	

	
</script>

<div id="page-wrapper">   
	<h1>
		Entri Barang Keluar
	</h1>       
	<ol class="breadcrumb">
		<li><i class="fa fa-home"></i> Barang Keluar </li>
	</ol>

	<!-- Main content -->
	<section class="content">
		<div class="modal-body" > 
			<div id="callout-navbar-btn-context " class="bs-callout bs-callout-info">
				<div class="row">
					<div class="col-md-12" >
						<div class="panel panel-primary">
							<div class="panel-body">
								<div class="form-inline">
									<div class="form-group">
											<label>Jenis Donasi</label>
											<select required id="txtJenisdonasi" style="text-transform: uppercase; width: 100%" class="form-control fields1">
												<!-- <option disabled selected value=""></option> -->
											</select>
									</div>
									<div class="form-group">
											<label>Sub Jenis Donasi</label>
											<select required id="txtSubjenisdonasi" style="text-transform: uppercase; width: 100%" class="form-control" disabled="">
												<!-- <option disabled selected value=""></option> -->
											</select>
									</div>
									
								</div>
								<div class="form-group">
								  <label>Kategori Barang</label>
								  <select required id="txtKategoribarang" class="form-control">
								  	<option disabled selected value=""></option>
								  </select>
					            </div> 
								
								<div class="form-group">
									<label>Nama Barang</label>
									
									<select name="namabarang" id="namabarang" style="width: 95%;" class=" form-control fields1" > 
						  			</select>
								</div>
								<div class="form-inline">
								
									<div class="form-group" >
										<label>Jumlah</label>
										<input name="jumlah" id="jumlah"  type="text" class="form-control fields1"  aria-label="..." required>
									</div>
									<div class="form-group" >
										<label>Stok</label>
										<input name="stok" id="stok" type="text" class="form-control"  aria-label="..." disabled>
									</div>
								
									<div class="form-group" >
										<label>Satuan</label>
										<input name="satuan" id="satuan"  type="text" class="form-control"  aria-label="..." disabled="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class="col-md-12">
						<br>
						<div class="form-group">
							<label>Jenis Penerima</label>
							<select name="jenispenerima" id="jenispenerima" style="width: 95%;" class=" form-control fields1" > 
								<option></option>
								<option value="INTERNAL">INTERNAL</option>
								<option value="EKSTERNAL">EXTERNAL</option>
				  			</select>
						</div>
					</div>	
					
					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-body">	
								<label>Nama</label>
								<input name="nama" id="nama"  type="text" class="form-control fields1"  aria-label="...">
								<label>Instansi / Unit</label>
								<select name="instansi" id="instansi" style="width: 95%;" class=" form-control fields1" > 
									<option disabled selected value=""></option>
					  			</select>
								
								
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-primary" >
							<div class="panel-body" >	
								<label>Alamat</label>
								<input name="alamat" id="alamat"  type="text" class="form-control"  aria-label="..." disabled="">
								<label>KTP</label>
								<input name="ktp" id="ktp"  type="text" class="form-control "  aria-label="..." disabled="">
							</div>
						</div>
					</div>
					
					<div class="col-md-12">

						<div class="form-group">
							<label>Pengiriman</label>
							<select name="pengiriman" id="pengiriman" style="width: 95%;" class=" form-control fields1" > 
								<option >  </option>
								<option value="KURIR"> Kurir </option>
								<option value="LANGSUNG TERIMA"> Langsung Terima </option>
				  			</select>
						</div>
						<div class="form-group">
							<label>Kondisi Barang</label>
							<select name="kondisibarang" id="kondisibarang" style="width: 95%;" class=" form-control fields1" > 
								<option >  </option>
								<option value="LAYAK"> LAYAK </option>
								<option value="TIDAK LAYAK"> TIDAK LAYAK </option>
				  			</select>
						</div>
						<div class="form-group">
							<label>Tanggal Distribusi</label>
							<div class="input-group date" style="width: 50%">
			                  <input type="text" class="form-control fields1" placeholder="Tanggal" name="tanggaldistribusi" id="tanggaldistribusi" />
			                  <span class="input-group-addon">
			                      <span class="glyphicon glyphicon-calendar"></span>
			                  </span>
			              	</div>
						</div>
						<div class="form-group">
							<label>Keterangan</label>
							<table>
								<tr>
									<td>
										<textarea name="keterangan" id="keterangan" style="width: 150%" >
										</textarea>
									</td>
								</tr>
							
							</table>
						</div>	
						
					    <div>
							<button type="button" id="btnsimpan" class="cari btn btn-success pull-left"  >Simpan</button>
						</div>
						<br>
						<br>
						<br>
					</div>	

					<div class="form-inline" >
						<div class="col-md-12">
							<div class="form-group">
				              <div class="input-group date" >
				                  <input type="text" class="form-control" placeholder="Tanggal" name="txtTanggalAwal" id="txtTanggalAwal" value="<?php echo date('d-m-Y'); ?>" />
				                  <span class="input-group-addon">
				                      <span class="glyphicon glyphicon-calendar"></span>
				                  </span>
				              </div>
				            </div>
						
						
							<div class="form-group">
				              <div class="input-group date" >
				                  <input type="text" class="form-control" placeholder="Tanggal" name="txtTanggalAkhir" id="txtTanggalAkhir" value="<?php echo date('d-m-Y'); ?>" />
				                  <span class="input-group-addon">
				                      <span class="glyphicon glyphicon-calendar"></span>
				                  </span>
				              </div>
				            </div>
						
						
							<div class="form-group">
								<button class="btn btn-primary" id="btnPerbaharui">Perbaharui Data</button>
							</div>
						</div>
						

					</div>
				</div>
				<br>
				<table id="tabelList1" style="border-top-style: solid;border-top-width: 0px;margin-top: 20px;" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr class="bg-info">
							<th></th>
							<th>Batal</th>
							<th width="10%">Id</th>
							<th>Jenis Donasi</th>
							<th>Sub Jenis Donasi</th>
							<th>Nama Kategori</th>
							<th>Nama Barang</th>	
							<th>Jumlah</th>
							<th>Satuan</th>
							<th>Jenis Penerima</th>
							<th>Nama</th>
							<th>Alamat</th>
							<th>KTP</th>
							<th>Instansi</th>
							<th>Tanggal Distribusi</th>
							<th>Kondisi Barang</th>
							<th>Pengiriman</th>
							<th>Keterangan</th>
							<th>Tanggal Input</th>
							<th>User Input</th>
						</tr>
					</thead>

					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	</section><!-- /.content -->
	
</div><!-- /#page-wrapper -->

</body>

</html>

