<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class barangkeluar extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('barangkeluar_model');

    }
	private function _render($view,$data = array())
	{
		$this->load->view('header',$data);
	    $this->load->view('body_header');
	    $this->load->view('sidebar');
	    $this->load->view($view,$data);
	    $this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');
	  	redirect('index.php/barangkeluar/barangkeluarlist', $data);
	}

	public function barangkeluarlist()
	{
		//---------------------------------------------------------------
		//load library
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		
		$this->_render('barangkeluar_view');
		
	}

	
	public function databarang()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangkeluar_model');


			$parameter1       = $this->input->post('parameter1');
			$idjenisdonasi       = $this->input->post('idjenisdonasi');
			$idsubjenisdonasi       = $this->input->post('idsubjenisdonasi');
			$idkategoribarang       = $this->input->post('idkategoribarang');
			$query=$this->barangkeluar_model->get_databarang($parameter1,$idjenisdonasi,$idsubjenisdonasi,$idkategoribarang);
			$data 			= array();
			foreach($query->result() as $hasil){
				$data[] 	= array(
					"id" 	=> $hasil->ID_MBARANG,
					/*"text" 	=> $hasil->KODE.' - '.$hasil->BARANG_FULL.' - STOK '.$hasil->STOK,*/
					"text" 	=> $hasil->NAMA_BARANG,
					"stok" 	=> $hasil->STOK,
					"satuan" 	=> $hasil->SATUAN

					
				);
			}
						
			echo json_encode($data);
			
	}
	public function datatujuan()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangkeluar_model');


			$parameter1       = $this->input->post('parameter1');
			$query=$this->barangkeluar_model->get_datatujuan($parameter1);
			$data 			= array();
			foreach($query->result() as $hasil){
				$data[] 	= array(
					"id" 	=> $hasil->NAMA_TUJUAN,
					/*"text" 	=> $hasil->KODE.' - '.$hasil->BARANG_FULL.' - STOK '.$hasil->STOK,*/
					"text" 	=> $hasil->NAMA_TUJUAN
					
				);
			}
						
			echo json_encode($data);
			
	}
	public function tabelbarangkeluar()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangkeluar_model');
		

		$tanggalawal   = date('Y-m-d', strtotime($this->input->post('tanggalawal')));
		$tanggalakhir  = date('Y-m-d', strtotime($this->input->post('tanggalakhir')));
		$query=$this->barangkeluar_model->get_datalist($tanggalawal,$tanggalakhir);
		
		$data 			= array();
		foreach($query->result() as $kolom){
			if ($kolom->BTNBATAL==1)
			{
			$btn='<button type="button" id="btnbatal" class="cari btn btn-danger pull-left" onclick="bataltransaksi('.$kolom->ID_KELUAR.')">Batal</button>';
			}
			else
			{
				$btn='Sudah Cetak';
			}

			if ($kolom->AKTIF==0)
			{
				$btn='Batal';	
			}
			$data[] 	= array(
				"kosong" 		=> "",
				"namajenisdonasi" 		=> $kolom->NAMA_JENISDONASI,
				"subnamajenisdonasi" 		=> $kolom->NAMA_JENISDONASI_DET,
				"idkeluar" 		=> $kolom->ID_KELUAR,
				"namabarang" 	=> $kolom->NAMA_BARANG,
				"jenispenerima"	=> $kolom->NAMA_JENISPENERIMA,
				"nama"			=> $kolom->NAMA,
				"alamat"		=> $kolom->ALAMAT,
				"ktp"			=> $kolom->KTP,
				"instansi" 		=> $kolom->INSTANSI,
				"tanggaldistribusi" => date('d-m-Y',strtotime($kolom->TANGGAL_DISTRIBUSI)),
				"pengiriman" 	=> $kolom->NAMA_JENISPENGIRIMAN,
				"kondisibarang" 	=> $kolom->KONDISIBARANG,
				"keterangan" 	=> $kolom->KETERANGAN,
				"jumlah" 		=> $kolom->JUMLAH,
				"namasatuan" 	=> $kolom->NAMA_SATUAN,
				"namakategori" 	=> $kolom->NAMA_KATEGORI,
				"btnbatal"		=> $btn,
				"tanggalinput"	=> date('d-m-Y H:i:s',strtotime($kolom->TANGGALINPUT)),
				"userinput"		=> $kolom->NAMAUSER


			);
		}

		$obj 			= array(
			"data" 			=> $data
		);
		
		echo json_encode($obj);
	}
	public function simpanbarangkeluar()
	{
		//---------------------------------------------------------------
		//load library
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->model('barangkeluar_model');
		
		$idjenisdonasi = $this->input->post('idjenisdonasi');	
		$idsubjenisdonasi = $this->input->post('idsubjenisdonasi');	
		$idkategori = $this->input->post('idkategori');	
		$namabarang = strtoupper($this->input->post('namabarang'));		
		$jumlah		= strtoupper($this->input->post('jumlah'));
		$jenispenerima	= strtoupper($this->input->post('jenispenerima'));
		$nama		= strtoupper($this->input->post('nama'));
		$alamat		= strtoupper($this->input->post('alamat'));
		$instansi	= strtoupper($this->input->post('instansi'));
		$ktp		= strtoupper($this->input->post('ktp'));
		$pengiriman	= strtoupper($this->input->post('pengiriman'));
		$tanggaldistribusi	= date('Y-m-d',strtotime($this->input->post('tanggaldistribusi')));
		$kondisibarang	= strtoupper($this->input->post('kondisibarang'));
		$keterangan	= rtrim(ltrim($this->input->post('keterangan')));
		$satuan	= rtrim(ltrim($this->input->post('satuan')));
		$user		= $this->session->userdata("ses_keterangan");
	
		$query=$this->barangkeluar_model->set_simpanbarangkeluar($idjenisdonasi,$idsubjenisdonasi,$idkategori,$namabarang,$jumlah,$jenispenerima,$nama,$alamat,$instansi,$ktp,$pengiriman,$tanggaldistribusi,$kondisibarang,$keterangan,$satuan,$user);

		$data['ALERT']=$query->row()->ALERT;
		echo json_encode($data);
			
	}
	public function datajenisdonasi()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangkeluar_model');

			$query=$this->barangkeluar_model->get_datajenisdonasi();
			$opt="<option value=''></option>";
			foreach($query as $data){
				$opt 		.= "<option value='".$data->ID_JENISDONASI."'>".$data->NAMA_JENISDONASI."</option>";
			}

			echo $opt;
			
	}

	public function datasubjenisdonasi()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangkeluar_model');

			$idjenisdonasi       = $this->input->post('idjenisdonasi');
			$query=$this->barangkeluar_model->get_datasubjenisdonasi($idjenisdonasi);
			$opt="<option value=''></option>";
			foreach($query as $data){
				$opt 		.= "<option value='".$data->ID_DET_JENISDONASI."'>".$data->NAMA_JENISDONASI_DET."</option>";
			}

			echo $opt;
			
	}
	public function datakategori()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangkeluar_model');


			$parameter1       = $this->input->post('parameter1');
			$query=$this->barangkeluar_model->get_datakategori($parameter1);
			$data 			= array();
			foreach($query as $hasil){
				$data[] 	= array(
					"id" 	=> $hasil->ID_KATEGORI,
					"text" 	=> $hasil->NAMA_KATEGORI
					
				);
			}
						
			echo json_encode($data);
			
	}
	public function datainstansi()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangkeluar_model');


			$parameter1       = $this->input->post('parameter1');
			$query=$this->barangkeluar_model->get_datainstansi($parameter1);
			$data 			= array();
			foreach($query as $hasil){
				$data[] 	= array(
					"id" 	=> $hasil->NAMA_INSTANSI,
					"text" 	=> $hasil->NAMA_INSTANSI
					
				);
			}
						
			echo json_encode($data);
			
	}
	public function bataltransaksi()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->model('barangkeluar_model');


			$urutan       = $this->input->post('urutan');
			$user = $this->session->userdata('ses_keterangan');

			$query=$this->barangkeluar_model->set_bataltransaksi($urutan,$user);
			
			$data 			= array();
			$data['ALERT']=$query->row()->ALERT;
			echo json_encode($data);
			
	}
	


}
