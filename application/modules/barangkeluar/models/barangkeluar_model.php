<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class barangkeluar_model extends CI_Model{

  /*private $db2;*/
	public function __construct()
  {
      // Call the CI_Model constructor
      parent::__construct();
      //$this->db2 = $this->load->database('sirs', TRUE);

  }
  
  
  /*public function get_list()
  {
    $this->load->database();
    $sql = "SELECT top 10 no,nomoradmedika,namapasien,namafaskes,kelas,tanggalterimaberkas FROM datatagihanreimbursement ORDER BY no desc " ;
    return $this->db->query($sql);    
  }*/
  


  public function get_databarang($parameter,$idjenisdonasi,$idsubjenisdonasi,$idkategoribarang)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_barangkeluar_databarang '$parameter','$idjenisdonasi','$idsubjenisdonasi','$idkategoribarang'";
    return $this->db->query($sql);    
  }
  public function get_datatujuan($parameter)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_barangkeluar_datatujuan '$parameter'";
    return $this->db->query($sql);    
  }
  public function get_datalist($tanggalawal,$tanggalakhir)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_barangkeluar_datalistbarangkeluar '$tanggalawal','$tanggalakhir'";
    return $this->db->query($sql);    
  }
   public function set_simpanbarangkeluar($idjenisdonasi,$idsubjenisdonasi,$idkategori,$namabarang,$jumlah,$jenispenerima,$nama,$alamat,$instansi,$ktp,$pengiriman,$tanggaldistribusi,$kondisibarang,$keterangan,$satuan,$user)
  {
    $this->load->database();
    $keterangan=$this->db->escape($keterangan); 
    $sql = "exec pengadaan..don_barangkeluar_simpan '$idjenisdonasi','$idsubjenisdonasi','$idkategori','$namabarang','$jumlah','$jenispenerima','$nama','$alamat','$instansi','$ktp','$pengiriman','$tanggaldistribusi','$kondisibarang',$keterangan,'$satuan','$user'";
    return $this->db->query($sql);    
  }
   public function get_datajenisdonasi()
  {
    $this->load->database();
    $sql = "select ID_JENISDONASI,NAMA_JENISDONASI,CRTDT,CRTUSR from pengadaan..DON_JENISDONASI";
    return $this->db->query($sql)->result(); 
  }
  public function get_datasubjenisdonasi($idjenisdonasi)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_barangmasuk_datasubjenisdonasi '$idjenisdonasi'";
    return $this->db->query($sql)->result(); 
  }
  public function get_datakategori($param)
  {
    $this->load->database();
    $sql = "select ID_KATEGORI, NAMA_KATEGORI, CRTDT, CRTUSR from pengadaan..DON_KATEGORI where NAMA_KATEGORI like '%$param%'";
    return $this->db->query($sql)->result(); 
  }
  public function get_datainstansi($param)
  {
    $this->load->database();
    $sql = "select ID_INSTANSI, NAMA_INSTANSI, CRTDT, CRTUSR from pengadaan..DON_INSTANSI where NAMA_INSTANSI like '%$param%'";
    return $this->db->query($sql)->result(); 
  }
   public function set_bataltransaksi($urutan,$user)
  {
    $this->load->database();
    $sql = "exec pengadaan..don_barangkeluar_bataltransaksi '$urutan','$user'";
    return $this->db->query($sql); 
  }
 
 
    
}
