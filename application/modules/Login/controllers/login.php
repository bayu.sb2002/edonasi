<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

	private function _render($view,$data = array())
	{
		$this->load->view('header_login',$data);
	    //$this->load->view('body_header');
	    //$this->load->view('sidebar');
	    $this->load->view($view,$data);
	    //$this->load->view('footer');
	}

	public function index()
	{
		//load library
		$uri =& load_class('URI', 'core');

	  	redirect(base_url().'login/datalogin',$data);
	}

	public function datalogin()
	{
		//---------------------------------------------------------------
		//load library
		//phpinfo();

		
		$this->load->library('session');
		$this->session->unset_userdata('ses_kodepriv');
		$this->session->unset_userdata('ses_keterangan');
		$this->session->unset_userdata('ses_sidebar');
		$this->session->unset_userdata('ses_namauser');
		$this->session->unset_userdata('ses_leveluser');
		$this->_render('login_list');


	}
	function clean($string) 
	{
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

	   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}
	function get_client_ip() {
    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
    return $ipaddress;
	}
	public function getlogin()
	{
		$uri =& load_class('URI', 'core');
		$this->load->library('session');
		$this->load->model('login_model');
		$user   = $this->input->post('userid');
		$user = $this->clean($user);
		$pass   = $this->input->post('passid');
		$pass = $this->clean($pass);

		$this->simpanlog((substr($user,0,49)),(substr($pass,0,49)));
       
		if($this->login_model->getdatalogin($user,$pass) )
			{
				$query = $this->login_model->getdatalogin($user,$pass);
				$keterangan=$query->row()->KETERANGAN;
				if ($keterangan!="")
				{
					$datasesi = array(
					        'ses_keterangan' => $query->row()->KETERANGAN,
					        'ses_namauser' => $query->row()->NAMAUSER,
					        'ses_leveluser' => $query->row()->LEVELUSER

					);
					$this->session->set_userdata($datasesi);

					redirect(base_url().'index.php/beranda/data');
				}
				else
				{
					$this->session->set_flashdata('hasil',$query->row()->ALERT);
					redirect(base_url().'index.php/login',$data);


				}
			}		
		
			
		
		
		//echo json_encode($data);

	}
	public function simpanlog($user,$pass)
	{
		//---------------------------------------------------------------
		//load library
		
		$ip=$this->get_client_ip();
		$this->login_model->simpanlog($user,$pass,$ip);
	}

	

	
}
