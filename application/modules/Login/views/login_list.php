<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('session');
?>
<style type="text/css">
	blink {
    -webkit-animation: 1s linear infinite condemned_blink_effect; // for android
    animation: 1s linear infinite condemned_blink_effect;
}
@-webkit-keyframes condemned_blink_effect { // for android
    0% {
        visibility: hidden;
    }
    50% {
        visibility: hidden;
    }
    100% {
        visibility: visible;
    }
}
@keyframes condemned_blink_effect {
    0% {
        visibility: hidden;
    }
    50% {
        visibility: hidden;
    }
    100% {
        visibility: visible;
    }
}
</style>
<style type="text/css">
	body{
	margin:0;
	color:#6a6f8c;
	background:#c8c8c8;
	font:600 16px/18px 'Open Sans',sans-serif;
}
*,:after,:before{-webkit-box-sizing:border-box;box-sizing:border-box}
.clearfix:after,.clearfix:before{content:'';display:table}
.clearfix:after{clear:both;display:block}
a{color:inherit;text-decoration:none}

.login-wrap{
	width:100%;
	margin:auto;
	max-width:525px;
	min-height:670px;
	position:relative;
	background:url(<?php echo base_url().'image/rawatinap.jpg'; ?>) no-repeat center;
	-webkit-box-shadow:0 12px 15px 0 rgba(0,0,0,.24),0 17px 50px 0 rgba(0,0,0,.19);
	        box-shadow:0 12px 15px 0 rgba(0,0,0,.24),0 17px 50px 0 rgba(0,0,0,.19);
}
.login-html{
	width:100%;
	height:100%;
	position:absolute;
	padding:90px 70px 50px 70px;
	background:rgba(40,57,101,.9);
}
.login-html .sign-in-htm,
.login-html .sign-up-htm{
	top:0;
	left:0;
	right:0;
	bottom:0;
	position:absolute;
	-webkit-transform:rotateY(180deg);
	        transform:rotateY(180deg);
	-webkit-backface-visibility:hidden;
	        backface-visibility:hidden;
	-webkit-transition:all .4s linear;
	transition:all .4s linear;
}
.login-html .sign-in,
.login-html .sign-up,
.login-form .group .check{
	display:none;
}
.login-html .tab,
.login-form .group .label,
.login-form .group .button{
	text-transform:uppercase;
}
.login-html .tab{
	font-size:22px;
	margin-right:15px;
	padding-bottom:5px;
	margin:0 15px 10px 0;
	display:inline-block;
	border-bottom:2px solid transparent;
}
.login-html .sign-in:checked + .tab,
.login-html .sign-up:checked + .tab{
	color:#fff;
	border-color:#1161ee;
}
.login-form{
	min-height:345px;
	position:relative;
	-webkit-perspective:1000px;
	        perspective:1000px;
	-webkit-transform-style:preserve-3d;
	        transform-style:preserve-3d;
}
.login-form .group{
	margin-bottom:15px;
}
.login-form .group .label,
.login-form .group .input,
.login-form .group .button{
	width:100%;
	color:#fff;
	display:block;
}
.login-form .group .input,
.login-form .group .button{
	border:none;
	padding:15px 20px;
	border-radius:25px;
	background:rgba(255,255,255,.1);
}
.login-form .group input[data-type="password"]{
	text-security:circle;
	-webkit-text-security:circle;
}
.login-form .group .label{
	color:#aaa;
	font-size:12px;
}
.login-form .group .button{
	background:#1161ee;
}
.login-form .group label .icon{
	width:15px;
	height:15px;
	border-radius:2px;
	position:relative;
	display:inline-block;
	background:rgba(255,255,255,.1);
}
.login-form .group label .icon:before,
.login-form .group label .icon:after{
	content:'';
	width:10px;
	height:2px;
	background:#fff;
	position:absolute;
	-webkit-transition:all .2s ease-in-out 0s;
	transition:all .2s ease-in-out 0s;
}
.login-form .group label .icon:before{
	left:3px;
	width:5px;
	bottom:6px;
	-webkit-transform:scale(0) rotate(0);
	        transform:scale(0) rotate(0);
}
.login-form .group label .icon:after{
	top:6px;
	right:0;
	-webkit-transform:scale(0) rotate(0);
	        transform:scale(0) rotate(0);
}
.login-form .group .check:checked + label{
	color:#fff;
}
.login-form .group .check:checked + label .icon{
	background:#1161ee;
}
.login-form .group .check:checked + label .icon:before{
	-webkit-transform:scale(1) rotate(45deg);
	        transform:scale(1) rotate(45deg);
}
.login-form .group .check:checked + label .icon:after{
	-webkit-transform:scale(1) rotate(-45deg);
	        transform:scale(1) rotate(-45deg);
}
.login-html .sign-in:checked + .tab + .sign-up + .tab + .login-form .sign-in-htm{
	-webkit-transform:rotate(0);
	        transform:rotate(0);
}
.login-html .sign-up:checked + .tab + .login-form .sign-up-htm{
	-webkit-transform:rotate(0);
	        transform:rotate(0);
}

.hr{
	height:2px;
	margin:60px 0 50px 0;
	background:rgba(255,255,255,.2);
}
.foot-lnk{
	text-align:center;
}
</style>


<script >
	
	$(document).ready(function() {
		$('#userid').val('');
		$('#passid').val('');
		$('#userid').focus();
		$('#btnlogin').click(function(event){
    		//cekformlogin();
   		});
		
		

		
	});
	/*function cekformlogin(){
		var emptyFields = $('.fieldlogin').filter(function() {
		    return $(this).val() === "";
		}).length;

		if (emptyFields === 0) {
		  getlogin();
		} else {
		  swal("MOHON DILENGKAPI DAHULU");
		   
		  // return false;
		}
	}
	function getlogin(){
		
	   jQuery.ajax({
	         type: "POST",
	         url: "getlogin", // the method we are calling
	         data:  {
	       	 			user : $('#userid').val(),
       	 				pass : $('#passid').val()
       	 			},
	         dataType: 'json',
	         success: function (data) {
	         	//swal(data['session1']);
	         	if (data['alert'])
	         	{
	         		swal(data['alert']);
	         	}
	         	else
	         	{
	         		$('#userid').val('');
		         	$('#passid').val('');

		         	swal("berhasil");
	            }
	         	
	         },
	         error: function (xhr,status,error) {
	             swal(xhr.responseText);
	         }
		});
	}*/
	


</script>


  <div class="login-wrap">
  	<div class="login-html">
		<span class="badge badge-pill" style="background-color: white;">
		 <img src="<?php echo base_url().'image/ptphc.png'; ?>" style='background-color: white;' height="60" width="380"> 
		</span>
		<div>
			<br>
		  <center style="color: white">E - DONASI<br><a style="color: green; font-style: italic;"></a></center>
		  <br>
		  
		</div>
		<br>
		<br>
		
		<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">LOGIN</label>
		<input id="tab-2" type="radio" name="tab" class="sign-up" ><label style="display: none" for="tab-2" class="tab">Sign Up</label>
		<div class="login-form">
			<div class="sign-in-htm">
				<form action="<?php echo base_url();?>index.php/login/getlogin" method="POST" style="margin:0">
					<div class="group">
						<label for="user" class="label">Username</label>
						<input id="userid" value="" name="userid" type="text" class="input fieldlogin form-control">
					</div>
					<blink><label align="center" style="color: red"><?php echo $this->session->userdata('hasil') ?></label></blink>
					<div class="group">
						<label for="pass" class="label">Password</label>
						<input id="passid" value="" name='passid' type="password" class="input fieldlogin form-control">
					</div>
					
					<div class="group">
						<input id="btnlogin" name="btnlogin" type="submit" class="button" value="Sign In">

					</div>
				</form>
				<br>
				<div class="row">
                <!-- <div class="col-md-6 col-xs-6">
                    <h5><a href="https://www.instagram.com/rsphcsurabaya/" style="color:white;font-weight:bold"><i class="fa fa-instagram " style="font-size:20px;color:#00aced"></i>&nbsp;&nbsp;@rsphcsurabaya</a></h5>                                                                                    
                </div>
                <div class="col-md-6 col-xs-6">
                    <h5><a href="https://facebook.com/rsphcsby" style="color:white;font-weight:bold"><i class="fa fa-facebook-square" style="font-size:20px;color:#0060b4"></i> &nbsp;&nbsp; rsphc</a></h5>
                </div> -->
            	</div>
            <p style="font-size:13px;margin-top:20px;font-weight:bold">© 2020 - RS. PHC</p>
				<!-- <div class="foot-lnk">
					<a href="#forgot">Forgot Password?</a>
				</div> -->
			</div>
			<div class="sign-up-htm">
				<div class="group">
					<label for="user" class="label">Username</label>
					<input id="user" type="text" class="input">
				</div>
				<div class="group">
					<label for="pass" class="label">Password</label>
					<input id="pass" type="password" class="input" data-type="password">
				</div>
				<div class="group">
					<label for="pass" class="label">Repeat Password</label>
					<input id="pass" type="password" class="input" data-type="password">
				</div>
				<div class="group">
					<label for="pass" class="label">Email Address</label>
					<input id="pass" type="text" class="input">
				</div>
				<div class="group">
					<input type="submit" class="button" value="Sign Up">
				</div>
				<div class="hr">
				</div>
				<div class="foot-lnk">
					<label for="tab-1">Already Member?</label>
				</div>
			</div>
		</div>
	    
           
        
    	  
	</div>
</div>
  
  



</body>

</html>

