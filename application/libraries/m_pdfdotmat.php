<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class m_pdfdotmat {
    
    function m_pdf()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load($param=NULL)
    {
        include_once APPPATH.'/third_party/mpdf60/mpdf.php';
         
        if ($param == NULL)
        {
            $param = '"en-GB-x","A4","","",10,10,10,10,6,3';  
            $param1 = '"utf-8", array(150,150)';        
        }
         
        return new mPDF("utf-8", array(100,140));
    }
} 

?>